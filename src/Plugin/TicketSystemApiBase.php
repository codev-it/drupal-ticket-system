<?php

namespace Drupal\codev_ticket_system\Plugin;

use Drupal;
use Drupal\codev_ticket_system\Settings;
use Drupal\codev_utils\Helper\Field;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\DiffArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Base class for Ticket system api plugins.
 *
 * @noinspection PhpUnused
 */
abstract class TicketSystemApiBase extends PluginBase implements TicketSystemApiInterface {

  /**
   * Http client instance.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;

  /**
   * Retrieves the entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Returns a channel logger object.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = Drupal::httpClient();
    $this->entityTypeManager = Drupal::entityTypeManager();
    $this->logger = Drupal::logger(Settings::MODULE_NAME);
  }

  /**
   * {@inheritDoc}
   */
  public function connect(): TicketSystemApiInterface {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $data): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getStatusOptions(): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getImportanceOptions(): array {
    return [];
  }

  /**
   * Parse the request body.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *
   * @return string
   */
  protected function parseRespBody(ResponseInterface $response): string {
    $body = $response->getBody();
    $size = $body->getSize();
    return $body->read($size);
  }

  /**
   * Parse the request body as json.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *
   * @return array
   */
  protected function parseRespBodyJson(ResponseInterface $response): array {
    return Json::decode($this->parseRespBody($response) ?: '{}');
  }

  /**
   * Return the client get request data.
   *
   * @param string $uri
   *
   * @return string
   */
  protected function clientGet(string $uri): string {
    $response = $this->httpClient->get($uri);
    return $this->parseRespBody($response);
  }

  /**
   * Return the client get request json data.
   *
   * @param string $uri
   *
   * @return array
   */
  protected function clientGetJson(string $uri): array {
    return Json::decode($this->clientGet($uri) ?: '{}');
  }

  /**
   * Send a post request with data.
   *
   * @param string $uri
   * @param array  $data
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  protected function clientSet(string $uri, array $data): ResponseInterface {
    return $this->httpClient->post($uri, $data);
  }

  /**
   * Send a post request with json data.
   *
   * @param string $uri
   * @param array  $data
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  protected function clientSetJson(string $uri, array $data): ResponseInterface {
    return $this->clientSet($uri, ['json' => $data]);
  }

  /**
   * Send a put request with data.
   *
   * @param string $uri
   * @param array  $data
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  protected function clientUpdate(string $uri, array $data): ResponseInterface {
    return $this->httpClient->put($uri, $data);
  }

  /**
   * Send a put request with json data.
   *
   * @param string $uri
   * @param array  $data
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  protected function clientUpdateJson(string $uri, array $data): ResponseInterface {
    return $this->clientUpdate($uri, ['json' => $data]);
  }

  /**
   * Send a delete request.
   *
   * @param string $uri
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  protected function clientDelete(string $uri): ResponseInterface {
    return $this->httpClient->delete($uri);
  }

  /**
   * Check if new data are different.
   *
   * @param array|null                          $data
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param array                               $excludes
   * @param bool                                $only_exists_keys
   *
   * @return bool
   */
  protected function isDataDiff(?array $data, EntityInterface $entity, array $excludes = [], bool $only_exists_keys = TRUE): bool {
    $curr_data = Field::fmtCleanValue($this::API_DATA_FIEL, $entity, '');
    $curr_data_arr = Json::decode($curr_data) ?: [];
    $diff = DiffArray::diffAssocRecursive($curr_data_arr, $data);
    if (empty($curr_data_arr)) {
      return TRUE;
    }
    if ($only_exists_keys) {
      $diff_keys = array_diff_key($curr_data_arr, $data);
      $excludes = array_merge($excludes, array_keys($diff_keys));
    }
    foreach ($excludes as $exclude) {
      if (isset($diff[$exclude])) {
        unset($diff[$exclude]);
      }
    }
    return !empty($diff);
  }

  /**
   * Build mapping info.
   *
   * @param string $intern
   * @param string $extern
   *
   * @return string
   */
  protected function buildMapping(string $intern, string $extern): string {
    return sprintf('%s::%s', $intern, $extern);
  }

  /**
   * Parse mapping info.
   *
   * @param string $mapping
   *
   * @return array
   */
  protected function parseMapping(string $mapping): array {
    return explode('::', $mapping);
  }

  /**
   * Parse mapping info multiple.
   *
   * @param array $mapping_arr
   *
   * @return array
   */
  protected function parseMappingMultiple(array $mapping_arr): array {
    $ret = [];
    foreach ($mapping_arr as $mapping) {
      $parsed = $this->parseMapping($mapping);
      $ret[$parsed[0]] = $parsed[1];
    }
    return $ret;
  }

}
