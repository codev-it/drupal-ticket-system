<?php

namespace Drupal\codev_ticket_system\Plugin;

use Drupal\codev_ticket_system\Entity\Ticket;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Ticket system api plugins.
 *
 * @noinspection PhpUnused
 */
interface TicketSystemApiInterface extends PluginInspectionInterface {

  /**
   * Api data field name.
   */
  const API_DATA_FIEL = 'api_data';

  /**
   * Api id field name.
   */
  const API_ID_FIEL = 'api_id';

  /**
   * Api attachment field name.
   */
  const API_ATTACHMENT_IDS_FIEL = 'api_attachment_ids';

  /**
   * Api comment field name.
   */
  const API_COMMENT_IDS_FIEL = 'api_comment_ids';

  /**
   * Connect to the api endpoint.
   */
  public function connect(): TicketSystemApiInterface;

  /**
   * Form with additional settings.
   *
   * @param array $data
   *
   * @return array
   */
  public function buildForm(array $data): array;

  /**
   * Return a list of available status select options.
   *
   * @return array
   */
  public function getStatusOptions(): array;

  /**
   * Return a list of available importance select options.
   *
   * @return array
   */
  public function getImportanceOptions(): array;

  /**
   * Create new ticket.
   *
   * @param \Drupal\codev_ticket_system\Entity\Ticket $ticket
   */
  public function create(Ticket $ticket);

  /**
   * Update ticket.
   *
   * @param \Drupal\codev_ticket_system\Entity\Ticket $ticket
   */
  public function update(Ticket $ticket);

  /**
   * Delete ticket.
   *
   * @param \Drupal\codev_ticket_system\Entity\Ticket $ticket
   */
  public function delete(Ticket $ticket);

  /**
   * Synchronized ticket. Sync all data between ticket entity and api endpoint.
   *
   * @param \Drupal\codev_ticket_system\Entity\Ticket $ticket
   * @param array                                     $data
   *
   * @return bool
   */
  public function sync(Ticket $ticket, array $data): bool;

  /**
   * Return all tasks from the api.
   *
   * @return array
   */
  public function loadTasks(): array;

  /**
   * Return all tasks from the api by given ids.
   *
   * @param array $ids
   *
   * @return array
   */
  public function loadTasksByIds(array $ids): array;

}
