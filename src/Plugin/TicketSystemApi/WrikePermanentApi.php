<?php

namespace Drupal\codev_ticket_system\Plugin\TicketSystemApi;

use Drupal;
use Drupal\codev_ticket_system\Annotation\TicketSystemApi;
use Drupal\codev_ticket_system\Entity\Ticket;
use Drupal\codev_ticket_system\Plugin\TicketSystemApiBase;
use Drupal\codev_ticket_system\Plugin\TicketSystemApiInterface;
use Drupal\codev_ticket_system\Settings;
use Drupal\codev_utils\Helper\Field;
use Drupal\codev_utils\Helper\Utils;
use Drupal\comment\Entity\Comment;
use Drupal\Component\Serialization\Json;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Exception;
use GuzzleHttp\Client;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_pages.module
 * .
 */

/**
 * Class WrikeApi.
 *
 * @package      Drupal\codevit_ticket_system\Plugin\TicketSystemApi
 *
 * @TicketSystemApi(
 *   id = "wrike_permanent",
 *   label = @Translation("Wrike API permanent token"),
 *   access_data_fields = {
 *     "Token" = "textfield",
 *   }
 * )
 *
 * @noinspection PhpUnused
 */
class WrikePermanentApi extends TicketSystemApiBase {

  /**
   * {@inheritDoc}
   */
  public function connect(): TicketSystemApiInterface {
    $token = Utils::getArrayValue('token', $this->configuration, '');
    $config = $this->httpClient->getConfig();
    $config['base_uri'] = 'https://www.wrike.com/api/v4/';
    $config['headers']['Authorization'] = 'bearer ' . $token;
    $this->httpClient = new Client($config);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $data): array {
    $folders = $this->getFolders();
    return [
      '#type'          => 'select',
      '#title'         => t('Select the folder you want to sync'),
      '#options'       => $this->buildFolderOptions($folders),
      '#default_value' => $data,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getStatusOptions(): array {
    return [
      'New'       => t('New'),
      'Active'    => t('Active'),
      'Completed' => t('Completed'),
      'Deferred'  => t('Deferred'),
      'Cancelled' => t('Cancelled'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getImportanceOptions(): array {
    return [
      'High'   => t('High'),
      'Normal' => t('Normal'),
      'Low'    => t('Low'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function create(Ticket $ticket) {
    if ($folder = $this->configuration['form'] ?? NULL) {
      try {
        $uri = sprintf('folders/%s/tasks', $folder);
        $response = $this->clientSetJson($uri, $this->buildData($ticket));
        if ($response->getStatusCode() === 200) {
          $response_arr = $this->parseRespBodyJson($response);
          $data = Utils::getArrayValueNested('data.0', $response_arr);
          $id = Utils::getArrayValue('id', $data, '');

          $ticket->set($this::API_ID_FIEL, Utils::getArrayValue('id', $data, ''));

          if ($attachments = $this->createAttachments($id, $ticket) ?: NULL) {
            $ticket->set($this::API_ATTACHMENT_IDS_FIEL, $attachments);
          }

          if ($comments = $this->createComments($id, $ticket) ?: NULL) {
            $ticket->set($this::API_COMMENT_IDS_FIEL, $comments);
          }
        }
      } catch (Exception $exception) {
        $this->logger->warning($exception->getMessage());
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function update(Ticket $ticket) {
    if ($task_id = Field::fmtCleanValue($this::API_ID_FIEL, $ticket)) {
      try {
        $status = Field::fmtCleanValue('status', $ticket, '');
        $status_new = $status == array_keys($this->getStatusOptions())[0];
        $response = $this->clientUpdateJson('tasks/' . $task_id, $this->buildData($ticket, $status_new));
        if ($response->getStatusCode() === 200) {
          /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $field */
          $field = $ticket->get('attachments');
          $api_attachments = Field::fmtCleanValue($this::API_ATTACHMENT_IDS_FIEL, $ticket, [], FALSE);
          if (!$field->isEmpty() || !empty($api_attachments)) {
            $new_mapping = [];
            $mapping = $this->parseMappingMultiple($api_attachments);

            foreach ($field->referencedEntities() as $file) {
              if (key_exists($file->id(), $mapping)) {
                if ($data = $this->updateAttachment($mapping[$file->id()], $file)) {
                  $fid = Utils::getArrayValue('id', $data, '');
                  $new_mapping[] = $this->buildMapping($file->id(), $fid);
                }
                unset($mapping[$file->id()]);
              }
              else {
                if ($data = $this->createAttachment($task_id, $file)) {
                  $fid = Utils::getArrayValue('id', $data, '');
                  $new_mapping[] = $this->buildMapping($file->id(), $fid);
                }
              }
            }

            $this->deleteAttachments($mapping ?: []);

            $ticket->set($this::API_ATTACHMENT_IDS_FIEL, $new_mapping);
          }

          try {
            /** @var \Drupal\comment\CommentStorage $comment_storage */
            $comment_storage = $this->entityTypeManager->getStorage('comment');
            $comments = $comment_storage->loadThread($ticket, 'comments', 'full');
          } catch (Exception $exception) {
            $comments = [];
          }
          $api_comments = Field::fmtCleanValue($this::API_COMMENT_IDS_FIEL, $ticket, [], FALSE);

          if (!empty($comments) || !empty($api_comments)) {
            $new_mapping = [];
            $mapping = $this->parseMappingMultiple($api_comments);
            foreach ($comments as $comment) {
              if (!key_exists($comment->id(), $mapping)) {
                if ($data = $this->createComment($task_id, $comment)) {
                  $cid = Utils::getArrayValue('id', $data, '');
                  $new_mapping[] = $this->buildMapping($comment->id(), $cid);
                }
              }
              else {
                $cid = $mapping[$comment->id()];
                $new_mapping[] = $this->buildMapping($comment->id(), $cid);
              }
            }

            $ticket->set($this::API_COMMENT_IDS_FIEL, $new_mapping);
          }
        }
      } catch (Exception $exception) {
        $this->logger->warning($exception->getMessage());
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function delete(Ticket $ticket) {
    if ($task = Field::fmtCleanValue($this::API_ID_FIEL, $ticket)) {
      try {
        $this->clientDelete('tasks/' . $task);
      } catch (Exception $exception) {
        $this->logger->warning($exception->getMessage());
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function sync(Ticket $ticket, array $data): bool {
    if (empty($data)) {
      return FALSE;
    }
    try {
      if ($this->isDataDiff($data, $ticket)) {
        $curr_time = time();

        if ($ticket->isNew() && isset($data['comments'])) {
          unset($data['comments']);
        }

        $ticket->set($this::API_ID_FIEL, Utils::getArrayValue('id', $data, ''))
          ->set($this::API_DATA_FIEL, Json::encode($data))
          ->set('name', Utils::getArrayValue('title', $data, ''))
          ->set('status', Utils::getArrayValue('status', $data, ''))
          ->set('importance', Utils::getArrayValue('importance', $data, ''))
          ->set('description', [
            'format' => 'basic_html',
            'value'  => Utils::getArrayValue('description', $data, ''),
          ])
          ->set('created', strtotime(Utils::getArrayValue('createdDate', $data)) ?: $curr_time)
          ->set('changed', strtotime(Utils::getArrayValue('updatedDate', $data)) ?: $curr_time);

        $attachments = Field::fmtCleanValue($this::API_ATTACHMENT_IDS_FIEL, $ticket, [], FALSE);
        if (!empty($data['attachments']) || !empty($attachments)) {
          $new_mapping = [];
          $mapping = $this->parseMappingMultiple($attachments);
          $files = $ticket->get('attachments')->getValue();
          foreach ($data['attachments'] as $attachment) {
            $id = Utils::getArrayValue('id', $attachment, '');
            if (!in_array($id, $mapping)) {
              $file_name = Utils::getArrayValue('name', $attachment, '');
              if ($file = $this->downloadAttachment($id, $file_name)) {
                $new_mapping[] = $this->buildMapping($file->id(), $id);
                $files[]['target_id'] = $file->id();
              }
            }
            else {
              $fid = array_search($id, $mapping);
              $new_mapping[] = $this->buildMapping($fid, $mapping[$fid]);
              unset($mapping[$fid]);
            }
          }

          if (!empty($mapping)) {
            foreach ($files as $key => $item) {
              $fid = $item['target_id'];
              if (!empty($mapping[$fid])) {
                $file = $this->entityTypeManager
                  ->getStorage('file')
                  ->load($fid);
                if ($file instanceof File) {
                  $file->delete();
                }
                unset($files[$key]);
              }
            }
          }

          $ticket->set('attachments', $files)
            ->set($this::API_ATTACHMENT_IDS_FIEL, $new_mapping);
        }

        $comments = Field::fmtCleanValue($this::API_COMMENT_IDS_FIEL, $ticket, [], FALSE);
        if (!empty($data['comments']) || !empty($comments)) {
          $mapping = $this->parseMappingMultiple($comments);
          foreach ($data['comments'] as $comment) {
            $id = Utils::getArrayValue('id', $comment, '');
            if (!in_array($id, $mapping)) {
              /** @var Comment $comment_entity */
              $comment_entity = $this->entityTypeManager
                ->getStorage('comment')
                ->create([
                  'entity_type'  => 'ticket',
                  'entity_id'    => $ticket->id(),
                  'field_name'   => 'comments',
                  'comment_type' => 'ticket',
                  'comment_body' => [
                    'format' => 'basic_html',
                    'value'  => Utils::getArrayValue('text', $comment, ''),
                  ],
                  'status'       => 1,
                ]);
              $comment_entity->save();
              $comment_id = Utils::getArrayValue('id', $comment, '');
              $comments[] = $this->buildMapping($comment_entity->id(), $comment_id);
            }
          }
          $ticket->set($this::API_COMMENT_IDS_FIEL, $comments);
        }
        return TRUE;
      }
    } catch (Exception $exception) {
      $this->logger->warning($exception->getMessage());
    }
    return FALSE;
  }

  /**
   * Build a data array to from the entity.
   *
   * @param \Drupal\codev_ticket_system\Entity\Ticket $ticket
   * @param bool                                      $new
   *
   * @return array
   */
  private function buildData(Ticket $ticket, bool $new = TRUE): array {
    $data = [
      'title'       => $ticket->getName(),
      'importance'  => Field::fmtCleanValue('importance', $ticket, ''),
      'description' => Field::fmtCleanValue('description', $ticket, ''),
    ];
    if (!$new) {
      $data['status'] = Field::fmtCleanValue('status', $ticket);
    }
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function loadTasks(): array {
    $ret = [];
    if ($folder = $this->configuration['form'] ?? NULL) {
      try {
        $uri = sprintf('folders/%s/tasks', $folder);
        $response = $this->clientGetJson($uri);
        $data = Utils::getArrayValue('data', $response, []);

        foreach ($data as &$item) {
          $id = Utils::getArrayValue('id', $item, '');
          $this->buildAdditionalTaskInfos($item);
          $ret[$id] = $item;
        }

        return $ret;
      } catch (Exception $exception) {
        $this->logger->warning($exception->getMessage());
      }
    }
    return $ret;
  }

  /**
   * {@inheritDoc}
   */
  public function loadTasksByIds(array $ids): array {
    $ret = [];

    $steps = 100;
    for ($i = 0; $i < count($ids); $i = $i + $steps) {
      $ids_str = implode(',', array_slice($ids, $i, $steps) ?: []);

      try {
        $response = $this->clientGetJson('tasks/' . $ids_str);
        $data = Utils::getArrayValue('data', $response, []);

        foreach ($data as &$item) {
          $id = Utils::getArrayValue('id', $item, '');
          $this->buildAdditionalTaskInfos($item);
          $ret[$id] = $item;
        }

      } catch (Exception $exception) {
        $this->logger->warning($exception->getMessage());
      }
    }

    return $ret;
  }

  /**
   * Load additional task infos and append to data.
   */
  private function buildAdditionalTaskInfos(&$item) {
    $item['attachments'] = $this->loadAttachmentByTask($item);
    $item['comments'] = $this->loadCommentsByTask($item);
  }

  /**
   * Create attachment.
   *
   * @param string                   $task_id
   * @param \Drupal\file\Entity\File $file
   *
   * @return array|null
   */
  private function createAttachment(string $task_id, File $file): ?array {
    $uri = sprintf('tasks/%s/attachments', $task_id);
    $response = $this->clientSet($uri, $this->buildFileData($file));
    if ($response->getStatusCode() === 200) {
      $response_arr = $this->parseRespBodyJson($response);
      return Utils::getArrayValueNested('data.0', $response_arr);
    }
    return NULL;
  }

  /**
   * Create attachment multiple.
   *
   * @param string                                    $task_id
   * @param \Drupal\codev_ticket_system\Entity\Ticket $ticket
   *
   * @return array
   */
  private function createAttachments(string $task_id, Ticket $ticket): array {
    $ids = [];
    /** @var \Drupal\file\Plugin\Field\FieldType\FileFieldItemList $files */
    $files = $ticket->get('attachments');

    if ($files->isEmpty()) {
      return $ids;
    }

    foreach ($files->referencedEntities() ?: [] as $file) {
      if ($data = $this->createAttachment($task_id, $file)) {
        $id = Utils::getArrayValue('id', $data, '');
        $ids[] = $this->buildMapping($file->id(), $id);
      }
    }

    return $ids;
  }

  /**
   * Update attachment.
   *
   * @param string                   $attachment_id
   * @param \Drupal\file\Entity\File $file
   *
   * @return array
   */
  private function updateAttachment(string $attachment_id, File $file): array {
    $curr_attachment = $this->loadAttachmentById($attachment_id);
    $curr_size = Utils::getArrayValue('size', $curr_attachment, 0);
    if ($curr_size != $file->getSize()) {
      $response = $this->clientUpdate('attachments/' . $attachment_id, $this->buildFileData($file));
      if ($response->getStatusCode() === 200) {
        $response_arr = $this->parseRespBodyJson($response);
        return Utils::getArrayValueNested('data.0', $response_arr);
      }
    }
    return $curr_attachment;
  }

  /**
   * Delete attachment.
   *
   * @param string $attachment_id
   */
  private function deleteAttachment(string $attachment_id) {
    $this->clientDelete('attachments/' . $attachment_id);
  }

  /**
   * Delete attachment multiple.
   *
   * @param array $attachment_ids
   */
  private function deleteAttachments(array $attachment_ids) {
    foreach ($attachment_ids as $attachment_id) {
      $this->deleteAttachment($attachment_id);
    }
  }

  /**
   * Build a data array to from the file.
   *
   * @param \Drupal\file\Entity\File $file
   *
   * @return array
   */
  private function buildFileData(File $file): array {
    $absolute_path = Drupal::service('file_system')
      ->realpath($file->getFileUri());
    if (file_exists($absolute_path)) {
      return [
        'headers' => [
          'content-type'     => 'application/octet-stream',
          'X-Requested-With' => 'XMLHttpRequest',
          'X-File-Name'      => $file->label(),
        ],
        'body'    => file_get_contents($absolute_path),
      ];
    }
    return [];
  }

  /**
   * Create comment.
   *
   * @param string                         $task_id
   * @param \Drupal\comment\Entity\Comment $comment
   *
   * @return array|null
   */
  private function createComment(string $task_id, Comment $comment): ?array {
    $uri = sprintf('tasks/%s/comments', $task_id);
    $response = $this->clientSetJson($uri, $this->buildCommentData($comment));
    if ($response->getStatusCode() === 200) {
      $response_arr = $this->parseRespBodyJson($response);
      return Utils::getArrayValueNested('data.0', $response_arr);
    }
    return NULL;
  }

  /**
   * Create comment multiple.
   *
   * @param string                                    $task_id
   * @param \Drupal\codev_ticket_system\Entity\Ticket $ticket
   *
   * @return array
   */
  private function createComments(string $task_id, Ticket $ticket): array {
    $ids = [];

    if ($ticket->get('comments')->isEmpty()) {
      return $ids;
    }

    try {
      /** @var \Drupal\comment\CommentStorage $comment_storage */
      $comment_storage = $this->entityTypeManager->getStorage('comment');
      $comments = $comment_storage->loadThread($ticket, 'comments', 'full');

      foreach ($comments as $comment) {
        if ($data = $this->createComment($task_id, $comment)) {
          $id = Utils::getArrayValue('id', $data, '');
          $ids[] = $this->buildMapping($comment->id(), $id);
        }
      }

    } catch (Exception $e) {
      return $ids;
    }

    return $ids;
  }

  /**
   * Build a data array to from the comments.
   *
   * @param \Drupal\comment\Entity\Comment $comment
   *
   * @return array
   */
  private function buildCommentData(Comment $comment): array {
    $content = trim(Field::fmtCleanValue('comment_body', $comment, ''));
    if ($comment->label() != strip_tags($content)) {
      $content = sprintf('%s<br />%s', $comment->label(), $content);
    }
    return [
      'text'      => $content,
      'plainText' => FALSE,
    ];
  }

  /**
   * Return the folders as tree.
   *
   * @return array
   */
  private function getFolders(): array {
    $folders_response = $this->clientGetJson('folders');
    $folders_data = Utils::getArrayValue('data', $folders_response, []);
    $ret = [];
    $folders = [];
    $child_ids = [];
    foreach ($folders_data as $item) {
      $folders[$item['id']] = $item;
      if ($item['childIds']) {
        $child_ids = array_unique(array_merge($child_ids, $item['childIds']));
      }
    }

    foreach ($folders as $id => $item) {
      if (!in_array($id, $child_ids)) {
        $item['child'] = $this->buildFoldersRecursive($item['childIds'], $folders);
        $ret[] = $item;
      }
    }
    return $ret;
  }

  /**
   * Build the folder's tree recursive.
   *
   * @param array $arr
   * @param array $folders
   *
   * @return array
   */
  private function buildFoldersRecursive(array $arr, array $folders): array {
    $ret = [];
    foreach ($arr as $child) {
      if (!empty($folders[$child])) {
        $item = $folders[$child];
        if ($item['childIds']) {
          $item['child'] = $this->buildFoldersRecursive($item['childIds'], $folders);
        }
        $ret[] = $item;
      }
    }
    return $ret;
  }

  /**
   * Build the folders option list.
   *
   * @param array $arr
   *
   * @return array
   */
  private function buildFolderOptions(array $arr): array {
    $ret = [];
    $items = [];
    foreach ($arr as $item) {
      $has_child = !empty($item['child']);
      $title = $this->buildMapping($item['title'], $item['id']);
      $items[] = $title;
      if ($has_child) {
        $items = array_merge($items, $this->buildFolderOptionsRecursive($title, $item['child']));
      }
    }

    foreach ($items as $val) {
      $var_s = $this->parseMapping($val);
      $ret[$var_s[1]] = $var_s[0];
    }

    return $ret;
  }

  /**
   * Build the folders option list recursive.
   *
   * @param string $title
   * @param array  $arr
   *
   * @return array
   */
  private function buildFolderOptionsRecursive(string $title, array $arr): array {
    $ret = [];
    foreach ($arr as $item) {
      $title_s = $this->parseMapping($title);
      $curr_title = $this->buildMapping($item['title'], $item['id']);
      $new_title = sprintf('%s > %s', $title_s[0], $curr_title);
      $ret[] = $new_title;
      if (!empty($item['child'])) {
        $ret = array_merge($ret, $this->buildFolderOptionsRecursive($new_title, $item['child']));
      }
    }
    return $ret;
  }

  /**
   * Get attachment by id.
   *
   * @param string $attachment_id
   *
   * @return array
   */
  private function loadAttachmentById(string $attachment_id): array {
    $response = $this->clientGetJson('attachments/' . $attachment_id);
    return Utils::getArrayValueNested('data.0', $response, []);
  }

  /**
   * Get attachment by task.
   *
   * @param array $task
   *
   * @return array
   */
  private function loadAttachmentByTask(array $task): array {
    if ($id = Utils::getArrayValue('id', $task)) {
      $uri = sprintf('tasks/%s/attachments', $id);
      $response = $this->clientGetJson($uri);
      return Utils::getArrayValue('data', $response, []);
    }
    return [];
  }

  /**
   * Download attachment by id.
   *
   * @param string $attachment_id
   * @param string $file_name
   *
   * @return \Drupal\file\FileInterface|null
   */
  private function downloadAttachment(string $attachment_id, string $file_name): ?FileInterface {
    $uri = sprintf('attachments/%s/download', $attachment_id);
    $response = $this->clientGet($uri);
    return Settings::fileSaveData($file_name, $response);
  }

  /**
   * Get comments by task.
   *
   * @param array $task
   *
   * @return array
   */
  private function loadCommentsByTask(array $task): array {
    if ($id = Utils::getArrayValue('id', $task)) {
      $uri = sprintf('tasks/%s/comments', $id);
      $response = $this->clientGetJson($uri);
      return Utils::getArrayValue('data', $response, []);
    }
    return [];
  }

}
