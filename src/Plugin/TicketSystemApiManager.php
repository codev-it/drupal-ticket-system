<?php

namespace Drupal\codev_ticket_system\Plugin;

use Drupal\codev_ticket_system\Annotation\TicketSystemApi;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Traversable;

/**
 * Provides the Ticket system api plugin manager.
 */
class TicketSystemApiManager extends DefaultPluginManager {

  /**
   * Constructs a new TicketSystemApiManager object.
   *
   * @param \Traversable                                  $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface      $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/TicketSystemApi',
      $namespaces, $module_handler,
      TicketSystemApiInterface::class,
      TicketSystemApi::class
    );

    $this->alterInfo('ticket_system_api_info');
    $this->setCacheBackend($cache_backend, 'ticket_system_api_plugins');
  }

  /**
   * {@inheritDoc}
   */
  public function createInstance($plugin_id, array $configuration = []): TicketSystemApiInterface {
    /** @var \Drupal\codev_ticket_system\Plugin\TicketSystemApiInterface $instance */
    $instance = parent::createInstance($plugin_id, $configuration);
    return $instance->connect();
  }

}
