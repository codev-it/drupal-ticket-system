<?php

namespace Drupal\codev_ticket_system\Form;

use Drupal;
use Drupal\codev_ticket_system\FormAccessDataManager;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AdminSettingsForm.
 *
 * @noinspection PhpUnused
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'codev_ticket_system.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'codev_ticket_system_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $data = $config->getRawData();
    $site_settings = Drupal::config('system.site')->getRawData();
    $notification = Utils::getArrayValue('notification', $data, []);
    $modal_settings = Utils::getArrayValue('entity_modal', $data, []);
    $api_access_data_count = 0;
    $api_access_data_widgets = [];

    $form['#tree'] = TRUE;

    $form['api_access_data'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Global API access data'),
      '#attributes' => ['id' => 'api-access-data'],
      '#open'       => TRUE,
    ];

    $form['api_access_data']['description'] = [
      '#type'  => 'html_tag',
      '#tag'   => 'p',
      '#value' => $this->t('These API data are global and can be reused in all ticket type entities.'),
    ];

    if ($form_state->isRebuilding()) {
      $form['api_access_data']['unsaved'] = [
        '#markup'     => sprintf(
          '<div class="messages messages--warning">%s</div>',
          $this->t('You have unsaved changes.')),
        '#attributes' => [
          'class' => [
            'view-changed',
            'messages',
            'messages--warning',
          ],
        ],
      ];
    }

    foreach ($data['api_access_data'] ?? [] as $access_data) {
      $api_access_data_widgets[$api_access_data_count] = $this->buildAccessDataMarkup(
        $api_access_data_count, $access_data);
      $api_access_data_count++;
    }

    for ($i = 0; $i < $form_state->get('api_access_data_append') ?: 0; $i++) {
      $api_access_data_widgets[$api_access_data_count] = $this->buildAccessDataMarkup(
        $api_access_data_count, []);
      $api_access_data_count++;
    }

    foreach ($form_state->get('api_access_data_unset') ?: [] as $key) {
      if (!empty($api_access_data_widgets[$key])) {
        unset($api_access_data_widgets[$key]);
      }
    }

    $form['api_access_data']['widgets'] = $api_access_data_widgets;

    $form['api_access_data']['append'] = [
      '#type'   => 'submit',
      '#value'  => $this->t('Add access data'),
      '#submit' => ['::addNewAccessDataElement'],
      '#ajax'   => [
        'callback' => '::updateAccessDataElement',
        'wrapper'  => 'api-access-data',
      ],
    ];

    $form['notification'] = [
      '#type'  => 'details',
      '#title' => $this->t('Ticket global notification settings'),
      '#open'  => TRUE,
    ];

    $form['notification']['enable'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Globally enable/disable notifications'),
      '#default_value' => $notification['enable'] ?? TRUE,
    ];

    $site_mail = Utils::getArrayValue('mail', $site_settings, '');
    $form['notification']['from'] = [
      '#type'          => 'email',
      '#title'         => $this->t('Mail from address'),
      '#default_value' => Utils::getArrayValue('from', $notification, $site_mail),
    ];

    $form['notification']['to'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Mail to addresses'),
      '#description'   => $this->t('Separate multiple addresses with a space or comma.'),
      '#default_value' => implode(', ', Utils::getArrayValue('to', $notification, [])),
    ];

    $form['notification']['reply'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Mail reply addresses'),
      '#description'   => $this->t('Separate multiple addresses with a space or comma.'),
      '#default_value' => implode(', ', Utils::getArrayValue('reply', $notification, [])),
    ];

    $form['modal'] = [
      '#type'  => 'details',
      '#title' => $this->t('Ticket entity view settings'),
    ];

    $form['modal']['enable'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Open entities in a modal window'),
      '#default_value' => Utils::getArrayValue('enable', $modal_settings, FALSE),
    ];

    $form['modal']['width'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Modal window size'),
      '#default_value' => Utils::getArrayValue('width', $modal_settings, 840),
      '#states'        => [
        'visible' => [
          ':input[name="modal[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $values = $form_state->getValues();
    $notification = Utils::getArrayValue('notification', $values, []);
    $notification_to = Utils::getArrayValue('to', $notification, '');
    $notification_reply = Utils::getArrayValue('reply', $notification, '');
    /** @var \Drupal\Component\Utility\EmailValidator $mail_validator */
    $mail_validator = Drupal::service('email.validator');

    foreach (Utils::strSplitValues($notification_to) as $mail) {
      if (!$mail_validator->isValid($mail)) {
        $form_state->setError($form['notification']['to']);
      }
    }

    foreach (Utils::strSplitValues($notification_reply) as $mail) {
      if (!$mail_validator->isValid($mail)) {
        $form_state->setError($form['notification']['reply']);
      }
    }

    if (!empty($triggering_element['#submit'])
      && in_array('::deleteAccessDataElement', $triggering_element['#submit'])) {
      $form_state->clearErrors();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config($this->getEditableConfigNames()[0]);
    $values = $form_state->getValues();
    $api_access_data = Utils::getArrayValue('api_access_data', $values, []);
    $widgets = Utils::getArrayValue('widgets', $api_access_data, []);
    $notification = Utils::getArrayValue('notification', $values, []);
    $api_data = [];

    foreach ($widgets as $data) {
      $widget = $data['widget'];
      $api = Utils::getArrayValue('api_select', $widget, '');
      $values = Utils::getArrayValue($api, $widget, []);
      $api_data_values = ['api' => $api];
      foreach ($values as $key => $value) {
        $id = str_replace('api_', '', $key);
        $api_data_values[$id] = $value;
      }
      $api_data[] = $api_data_values;
    }

    $config->set('api_access_data', $api_data)
      ->set('notification', [
        'enable' => $notification['enable'] ?? TRUE,
        'from'   => Utils::getArrayValue('from', $notification, ''),
        'to'     => Utils::strSplitValues(Utils::getArrayValue('to', $notification, '')),
        'reply'  => Utils::strSplitValues(Utils::getArrayValue('reply', $notification, '')),
      ])
      ->set('entity_modal', $form_state->getValue('modal') ?: [])
      ->save();
  }

  /**
   * Access data data markup.
   *
   * @param int   $id
   * @param array $data
   *
   * @return array
   */
  private function buildAccessDataMarkup(int $id, array $data = []): array {
    $field_name = sprintf('api_access_data[widgets][%s][widget][api_select]', $id);
    $name = Utils::getArrayValue('name', $data, t('New'));
    return [
      '#type'  => 'fieldset',
      '#title' => $this->t('API access data: @name', ['@name' => $name]),
      'widget' => FormAccessDataManager::buildWidget($data, $field_name),
      'remove' => [
        '#type'       => 'submit',
        '#name'       => $id,
        '#value'      => $this->t('Remove access data'),
        '#submit'     => ['::deleteAccessDataElement'],
        '#ajax'       => [
          'callback' => '::updateAccessDataElement',
          'wrapper'  => 'api-access-data',
        ],
        '#attributes' => ['class' => ['button--danger']],
      ],
    ];
  }

  /**
   * Update the access data form element callback.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function updateAccessDataElement(array $form, FormStateInterface $form_state): array {
    return $form['api_access_data'];
  }

  /**
   * Add new access data element item.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function addNewAccessDataElement(array &$form, FormStateInterface $form_state) {
    $api_access_data_append = $form_state->get('api_access_data_append') ?: 0;
    $form_state->set('api_access_data_append', $api_access_data_append + 1);
    $form_state->setRebuild();
  }

  /**
   * Delete access data element item.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function deleteAccessDataElement(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $unset_id = $triggering_element['#name'];
    $api_access_data_unset = $form_state->get('api_access_data_unset') ?: [];
    if (!in_array($unset_id, $api_access_data_unset)) {
      $api_access_data_unset[] = $unset_id;
    }
    $form_state->set('api_access_data_unset', $api_access_data_unset);
    $form_state->setRebuild();
  }

}
