<?php

namespace Drupal\codev_ticket_system\Form;

use Drupal;
use Drupal\codev_ticket_system\Entity\TicketType;
use Drupal\codev_ticket_system\FormAccessDataManager;
use Drupal\codev_ticket_system\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TicketTypeForm.
 *
 * @noinspection PhpUnused
 */
class TicketTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    /** @var TicketType $entity */
    $entity = $this->entity;
    $api_data = $entity->getApiData();

    $form['label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Label'),
      '#maxlength'     => 255,
      '#default_value' => $entity->label(),
      '#description'   => $this->t('Label for the Ticket type.'),
      '#required'      => TRUE,
    ];

    $form['id'] = [
      '#type'          => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name'  => [
        'exists' => [TicketType::class, 'load'],
      ],
      '#disabled'      => !$entity->isNew(),
    ];

    $form['description'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Description'),
      '#default_value' => $entity->getDescription(),
      '#description'   => $this->t('Description for the Ticket type.'),
    ];

    $form['notification'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Notification'),
      '#default_value' => $entity->isNotificationEnable(),
      '#description'   => $this->t('Enable/Disable E-Mail notification.'),
    ];

    $form['notification_to_str'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Mail to addresses'),
      '#description'   => $this->t('Separate multiple addresses with a space or comma.'),
      '#default_value' => implode(', ', $entity->getNotificationTo()),
      '#states'        => [
        'visible' => [
          ':input[name="notification"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['notification_reply_str'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Mail reply addresses'),
      '#description'   => $this->t('Separate multiple addresses with a space or comma.'),
      '#default_value' => implode(', ', $entity->getNotificationReply()),
      '#states'        => [
        'visible' => [
          ':input[name="notification"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['infos'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t("Default information's"),
      '#description'   => $this->t("This information's will be provided as standard by create an ticket."),
      '#options'       => [
        'browser'    => t('Attach browser info to ticket description.'),
        'screenshot' => t('Create and attach a screenshot. (Only available in modal mode.)'),
      ],
      '#default_value' => array_keys(array_filter($entity->getInformationHelper()) ?: []) ?: [],
    ];

    $form['api'] = [
      '#type'        => 'details',
      '#title'       => $this->t('API'),
      '#description' => $this->t('Select the API to connect with the ticket type.'),
      '#open'        => TRUE,
    ];

    $form['api']['select'] = [
      '#type'          => 'select',
      '#title'         => $this->t('API Name'),
      '#empty_option'  => t('-- None --'),
      '#options'       => $this->buildApiOptions(),
      '#default_value' => $api_data['name'] ?? '',
    ];

    $form['api']['access_data'] = [
      '#type'   => 'container',
      '#states' => [
        'visible' => [
          ':input[name="select"]' => ['value' => 'custom'],
        ],
      ],
      FormAccessDataManager::buildWidget($api_data,
        'api_select', [
          ':input[name="select"]' => ['value' => 'custom'],
        ], TRUE),
    ];

    if ($api = $entity->getApi()) {
      $form['api']['api_form']['#tree'] = TRUE;
      $form['api']['api_form'] += $api->buildForm($entity->getApiData() ?? []);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $notification_to = Utils::getArrayValue('notification_to', $values, '');
    $notification_reply = Utils::getArrayValue('notification_reply', $values, '');
    /** @var \Drupal\Component\Utility\EmailValidator $mail_validator */
    $mail_validator = Drupal::service('email.validator');

    foreach (Utils::strSplitValues($notification_to) as $mail) {
      if (!$mail_validator->isValid($mail)) {
        $form_state->setError($form['notification_to']);
      }
    }

    foreach (Utils::strSplitValues($notification_reply) as $mail) {
      if (!$mail_validator->isValid($mail)) {
        $form_state->setError($form['notification_reply']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $selected = Utils::getArrayValue('select', $values);
    $notification_to = Utils::getArrayValue('notification_to_str', $values);
    $notification_reply = Utils::getArrayValue('notification_reply_str', $values);
    $information_helper = Utils::getArrayValue('infos', $values, []);

    $api_data = [];
    if (!empty($selected)) {
      $api_data = ['name' => $selected];
      $needle = 'api_';
      foreach ($values as $key => $value) {
        if (strpos($key, $needle) !== FALSE) {
          $id = str_replace($needle, '', $key);
          $api_data[$id] = $value;
        }
      }
      $api_data['api'] = $api_data['select'];
      unset($api_data['select']);
    }

    foreach ($information_helper as &$info) {
      $info = !empty($info);
    }

    /** @var TicketType $entity */
    $entity = $this->getEntity();
    $entity->setNotificationTo(Utils::strSplitValues($notification_to));
    $entity->setNotificationReply(Utils::strSplitValues($notification_reply));
    $entity->setInformationHelper($information_helper);
    $entity->setApiData($api_data ?: []);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Ticket type.', [
            '%label' => $entity->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Ticket type.', [
            '%label' => $entity->label(),
          ]));
    }

    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }

  /**
   * Build the API data select options list.
   *
   * @return array
   */
  private function buildApiOptions(): array {
    $ret = [];
    $access_data = Settings::get('api_access_data') ?: [];
    $ret['custom'] = t('Custom');
    foreach ($access_data as $data) {
      $api = $data['api'] ?? '';
      $name = $data['name'] ?? '';
      $ret[$name] = sprintf('%s (%s)', $name, $api);
    }
    return $ret;
  }

}
