<?php

namespace Drupal\codev_ticket_system\Form;

use Drupal;
use Drupal\codev_ticket_system\Settings;
use Drupal\codev_utils\Ajax\DialogScrollTopCommand;
use Drupal\codev_utils\Ajax\ReloadCommand;
use Drupal\codev_utils\Helper\Field;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Ticket edit forms.
 *
 * @ingroup      codev_ticket_system
 *
 * @noinspection PhpUnused
 */
class TicketForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\codev_ticket_system\Entity\Ticket $entity */
    $entity = $this->getEntity();
    $bundle_entity = $entity->getBundleEntity();
    $api = $bundle_entity->getApi();
    $api_enable = $entity->isApiEnabled() && $api;
    $api_id = Field::fmtCleanValue('api_id', $entity, '');
    if ($api_enable && !empty($api_id)) {
      $tasks = $api->loadTasksByIds([$api_id]);
      $task = end($tasks);
      $api->sync($entity, $task);
    }

    $form = parent::buildForm($form, $form_state);

    if ($api_enable && !empty($api_id) && !empty($task)) {
      $description = Utils::getArrayValue('description', $task, '');
      $form['description']['widget'][0]['#default_value'] = $description;
    }

    $form['#attached']['library'][] = 'codev_utils/ajax.reload-command';
    $form['#attached']['library'][] = 'codev_utils/ajax.dialog-scroll-top-command';

    if (Drupal::request()->isXmlHttpRequest()) {
      $form['#prefix'] = sprintf('<div id="%s">', $this->getWrpId());
      $form['#suffix'] = '</div>';

      $form['actions']['submit']['#submit'][] = '::disableRedirectSubmit';

      $form['actions']['submit']['#ajax'] = ['callback' => '::updateCallback'];

      if (!empty($bundle_entity->getInformationHelper()['screenshot'])) {
        $form['screenshot'] = ['#type' => 'hidden'];
      }
    }

    if ($bundle_entity->isNotificationEnable()) {
      $form['notification'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Send notification mail'),
        '#default_value' => TRUE,
        '#weight'        => 99,
      ];
    }

    if ($api) {
      if (!empty($form['status']['widget'][0]['value'])
        && $status_options = $api->getStatusOptions() ?: NULL) {
        $form['status']['widget'][0]['value']['#type'] = 'select';
        $form['status']['widget'][0]['value']['#options'] = $status_options;
        unset($form['status']['widget'][0]['value']['#size']);
      }

      if (!empty($form['importance']['widget'][0]['value'])
        && $status_options = $api->getImportanceOptions() ?: NULL) {
        $form['importance']['widget'][0]['value']['#type'] = 'select';
        $form['importance']['widget'][0]['value']['#options'] = $status_options;
        unset($form['importance']['widget'][0]['value']['#size']);
      }
    }

    if ($entity->isNew()) {
      $form['status']['widget'][0]['value']['#value'] = t('New');
      $form['status']['widget'][0]['value']['#access'] = FALSE;

      if ($api && $status_options = $api->getStatusOptions() ?: NULL) {
        $val = array_keys($status_options)[0];
        $form['status']['widget'][0]['value']['#value'] = $val;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    /** @var \Drupal\codev_ticket_system\Entity\Ticket $entity */
    $entity = $this->getEntity();
    $bundle_entity = $entity->getBundleEntity();
    $info_helper = $bundle_entity->getInformationHelper();

    if (!empty($info_helper['browser'])) {
      $request = Drupal::request();
      $agent = $request->server->get('HTTP_USER_AGENT');
      $description = $entity->get('description')->first()->getValue();
      $description['value'] .= sprintf(
        '<br /><br /><strong>USER AGENT</strong><br />%s', $agent);
      $entity->set('description', $description);
    }

    if (!empty($info_helper['screenshot'])
      && $screenshot_base64 = $form_state->getValue('screenshot') ?: NULL) {
      $screenshot_data = explode(',', $screenshot_base64);
      $screenshot = base64_decode($screenshot_data[1]);
      $name = sprintf('screenshot-%s.png', date('Y-m-d'));
      if ($file = Settings::fileSaveData($name, $screenshot)) {
        $entity_files = $entity->get('attachments')->getValue() ?: [];
        array_unshift($entity_files, [
          'target_id' => $file->id(),
        ]);
        $entity->set('attachments', $entity_files);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\codev_ticket_system\Entity\Ticket $entity */
    $entity = $this->getEntity();
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $mail_key = 'create';
        $this->messenger()->addMessage($this->t('Created the %label Ticket.', [
          '%label' => $entity->label(),
        ]));
        break;
      default:
        $mail_key = 'update';
        $this->messenger()->addMessage($this->t('Saved the %label Ticket.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.ticket.canonical', [
      'ticket' => $entity->id(),
    ]);

    if ($form_state->getValue('notification')) {
      Settings::sendMail($entity, $mail_key);
    }
  }

  /**
   * Redirect submit.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function disableRedirectSubmit(array $form, FormStateInterface $form_state) {
    $form_state->disableRedirect();
  }

  /**
   * Form ajax update submit.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return AjaxResponse
   *
   * @noinspection PhpUnused
   */
  public function updateCallback(array $form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    $wrapper_id = '#' . $this->getWrpId();
    if (!$form_state->hasAnyErrors()) {
      $response->addCommand(new ReloadCommand());
    }
    else {
      $status_messages = ['#type' => 'status_messages'];
      $response->addCommand(new ReplaceCommand($wrapper_id, $form));
      $response->addCommand(new PrependCommand($wrapper_id, $status_messages));
      $response->addCommand(new DialogScrollTopCommand());
    }
    return $response;
  }

  /**
   * Build the form wrapper id.
   *
   * @return string
   */
  private function getWrpId(): string {
    return Html::cleanCssIdentifier($this->getFormId() . '-wrp');
  }

}
