<?php

namespace Drupal\codev_ticket_system\Form;

use Drupal;
use Drupal\codev_ticket_system\Settings;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting Ticket entities.
 *
 * @ingroup      codev_ticket_system
 *
 * @noinspection PhpUnused
 */
class TicketDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\codev_ticket_system\Entity\Ticket $entity */
    $entity = $this->getEntity();
    $entity_type = $entity->getBundleEntity();

    if ($entity_type->isNotificationEnable()) {
      $form['notification'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Send notification mail'),
        '#default_value' => TRUE,
        '#weight'        => 99,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);

    if (Drupal::request()->isXmlHttpRequest()) {
      $actions['cancel'] = [
        '#type'   => 'submit',
        '#value'  => $this->getCancelText(),
        '#submit' => ['::submitDisableRedirect'],
        '#ajax'   => ['callback' => '::closeDialogCallback'],
      ];
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\codev_ticket_system\Entity\Ticket $entity */
    $entity = $this->getEntity();

    if ($entity->isDefaultTranslation() && $form_state->getValue('notification')) {
      Settings::sendMail($entity, 'deleted');
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Redirect submit.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function submitDisableRedirect(array $form, FormStateInterface $form_state) {
    $form_state->disableRedirect();
  }

  /**
   * Redirect submit.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function closeDialogCallback(array $form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand());
    return $response;
  }

}
