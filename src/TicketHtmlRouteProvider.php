<?php

namespace Drupal\codev_ticket_system;

use Drupal\codev_ticket_system\Controller\TicketEntityController;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Ticket entities.
 *
 * @see          \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see          \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 *
 * @noinspection PhpUnused
 */
class TicketHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritDoc}
   */
  protected function getAddPageRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getAddPageRoute($entity_type)) {
      $route->setDefault('_controller', TicketEntityController::class . '::addPage');
      return $route;
    }
    return NULL;
  }

}
