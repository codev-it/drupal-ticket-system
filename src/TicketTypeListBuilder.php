<?php

namespace Drupal\codev_ticket_system;

use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Ticket type entities.
 *
 * @noinspection PhpUnused
 */
class TicketTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Name');
    $header['description'] = $this->t('Description');
    $header['notification'] = $this->t('Notification');
    $header['api'] = $this->t('API');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\codev_ticket_system\Entity\TicketType $entity */
    $name = $entity->getApiData()['name'] ?? '';
    $api_data = Settings::getAccessDataByName($name);
    if ($name === 'custom') {
      $name = t('Custom');
    }
    elseif (!empty($api_data)) {
      $api = Utils::getArrayValue('api', $api_data, '');
      $name = sprintf('%s (%s)', $name, $api);
    }
    $row['label'] = $entity->label();
    $row['description'] = $entity->getDescription();
    $row['notification'] = $entity->isNotificationEnable()
      ? t('Yes') : t('No');
    $row['api'] = $name ?: t('None');
    return $row + parent::buildRow($entity);
  }

}
