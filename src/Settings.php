<?php

namespace Drupal\codev_ticket_system;

use Drupal;
use Drupal\codev_ticket_system\Entity\Ticket;
use Drupal\codev_utils\SettingsBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Settings.php
 * .
 */

/**
 * Class Settings.
 *
 * @package      Drupal\codev_ticket_system
 */
class Settings extends SettingsBase {

  /**
   * Module name.
   *
   * @var string
   */
  public const MODULE_NAME = 'codev_ticket_system';

  /**
   * Clean identifier helper.
   *
   * @param string $identifier
   *
   * @return string
   */
  public static function cleanIdentifier(string $identifier = ''): string {
    return strtolower(Html::cleanCssIdentifier($identifier, [
      ' ' => '_',
      '_' => '_',
      '/' => '_',
      '[' => '_',
      ']' => '',
    ])) ?: '';
  }

  /**
   * Get access data by name.
   *
   * @param string $name
   *
   * @return array
   */
  public static function getAccessDataByName(string $name = ''): array {
    $ret = [];
    $data = static::get('api_access_data') ?: [];
    foreach ($data as $item) {
      if ($name === $item['name'] ?? '') {
        return $item;
      }
    }
    return $ret;
  }

  /**
   * Mail send shortcut.
   *
   * @param \Drupal\codev_ticket_system\Entity\Ticket $entity
   * @param string                                    $key
   *
   * @return array
   */
  public static function sendMail(Ticket $entity, string $key = 'create'): array {
    $entity_type = $entity->getBundleEntity();
    /** @var \Drupal\Core\Mail\MailManager $mail_manager */
    $mail_manager = Drupal::service('plugin.manager.mail');
    $params['entity'] = $entity;
    $module = static::MODULE_NAME;
    $langcode = Drupal::currentUser()->getPreferredLangcode();
    $to = implode(',', $entity_type->getNotificationTo());
    $reply = implode(',', $entity_type->getNotificationReply()) ?: NULL;
    return $mail_manager->mail($module, $key, $to, $langcode, $params, $reply, !empty($to));
  }

  /**
   * File create shortcut.
   *
   * @param string $name
   * @param string $data
   * @param int    $replace
   *
   * @return \Drupal\file\FileInterface|null
   */
  public static function fileSaveData(string $name, string $data, int $replace = FileSystemInterface::EXISTS_RENAME): ?FileInterface {
    /** @var \Drupal\Core\File\FileSystem $file_system */
    $file_system = Drupal::service('file_system');
    /** @var \Drupal\file\FileRepository $file_repository */
    $file_repository = Drupal::service('file.repository');
    $default_scheme = Drupal::config('system.file')->get('default_scheme');
    $directory = sprintf('%s://%s', $default_scheme, date('Y-m'));
    $destination = sprintf('%s/%s', $directory, $name);
    if ($file_system->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY)) {
      try {
        return $file_repository->writeData($data, $destination, $replace);
      } catch (Exception $exception) {
        return NULL;
      }
    }
    return NULL;
  }

}
