<?php

namespace Drupal\codev_ticket_system;

use Drupal;
use Drupal\codev_utils\Helper\Utils;

/**
 * Form manager for api access data fields.
 */
class FormAccessDataManager {

  /**
   * Return the api select option list.
   *
   * @param array $definitions
   *
   * @return array
   */
  public static function buildSelectOptions(array $definitions = []): array {
    $ret = [];
    $definitions = $definitions ?: static::getTicketSystemApiMangerDefinitions();
    foreach ($definitions as $key => $val) {
      $ret[$key] = $val['label'];
    }
    return $ret;
  }

  /**
   * Return the form widget presets for api access data.
   *
   * @param array  $data
   * @param string $form_name
   * @param array  $add_required_state
   * @param bool   $single
   *
   * @return array
   */
  public static function buildWidget(array $data = [], string $form_name = 'api_select', array $add_required_state = [], bool $single = FALSE): array {
    $widget = [];
    $definitions = static::getTicketSystemApiMangerDefinitions();
    $options = static::buildSelectOptions($definitions);
    $selected = Utils::getArrayValue('api', $data, '');

    $widget['api_select'] = [
      '#type'          => 'select',
      '#title'         => t('Choose the API type'),
      '#options'       => $options,
      '#default_value' => $selected,
      '#disabled'      => !$single && !empty($selected),
    ];

    foreach ($definitions as $api_key => $definition) {
      $widget[$api_key] = static::buildWidgetFields($api_key, $definitions, $data, $form_name, $add_required_state, $single);
    }

    return $widget;
  }

  /**
   * Return the ticket system api manger definitions.
   *
   * @return array
   */
  private static function getTicketSystemApiMangerDefinitions(): array {
    /** @var \Drupal\codev_ticket_system\Plugin\TicketSystemApiManager $ticket_system_api_manger */
    $ticket_system_api_manger = Drupal::service('plugin.manager.ticket_system_api');
    return $ticket_system_api_manger->getDefinitions() ?: [];
  }

  /**
   * Return the ticket system api access data fields.
   *
   * @param string $api
   * @param array  $definitions
   * @param array  $data
   * @param string $form_name
   * @param array  $add_required_state
   * @param bool   $single
   *
   * @return array
   */
  private static function buildWidgetFields(string $api, array $definitions = [], array $data = [], string $form_name = '', array $add_required_state = [], bool $single = FALSE): array {
    $name = Utils::getArrayValue('name', $data, '');

    $ret = [
      '#type'   => 'container',
      '#states' => [
        'visible' => [
          sprintf(':input[name="%s"]', $form_name) => ['value' => $api],
        ],
      ],
    ];

    if (!$single) {
      $ret['api_name'] = [
        '#type'          => 'textfield',
        '#title'         => t('API Name'),
        '#default_value' => $name,
        '#disabled'      => !empty($name),
        '#states'        => [
          'required' => [
            sprintf(':input[name="%s"]', $form_name) => ['value' => $api],
          ],
        ],
      ];

      if (!empty($add_required_state)) {
        $ret['api_name']['#states']['required'] += $add_required_state;
      }
    }

    $ret += static::buildAccessDataFields($api, $definitions, $data, $form_name, $add_required_state);

    return $ret;
  }

  /**
   * Return the access data fields.
   *
   * @param string $api
   * @param array  $definitions
   * @param array  $data
   * @param string $form_name
   * @param array  $add_required_state
   *
   * @return array
   */
  private static function buildAccessDataFields(string $api, array $definitions = [], array $data = [], string $form_name = '', array $add_required_state = []): array {
    $ret = [];
    $access_data_fields = $definitions[$api]['access_data_fields'] ?? [];
    foreach ($access_data_fields as $key => $val) {
      $id = Settings::cleanIdentifier($key);
      $arr_key = 'api_' . $id;
      $ret[$arr_key] = [
        '#type'          => $val,
        '#title'         => $key,
        '#default_value' => Utils::getArrayValue($id, $data, ''),
        '#maxlength'     => 512,
        '#states'        => [
          'required' => [
            sprintf(':input[name="%s"]', $form_name) => ['value' => $api],
          ],
        ],
      ];

      if (!empty($add_required_state)) {
        $ret[$arr_key]['#states']['required'] += $add_required_state;
      }
    }
    return $ret;
  }

}
