<?php

namespace Drupal\codev_ticket_system\Entity;

use Drupal;
use Drupal\codev_ticket_system\Settings;
use Drupal\codev_utils\Helper\Field;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Render\Element;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: TicketViewBuilder.php
 * .
 *
 * @noinspection PhpUnused
 */
class TicketViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritDoc}
   */
  public function viewMultiple(array $entities = [], $view_mode = 'full', $langcode = NULL): array {
    $build = parent::viewMultiple($entities, $view_mode, $langcode);

    $tasks = [];
    $api_data = [];
    $child_arr = Element::children($build);

    foreach ($child_arr as $child) {
      /** @var \Drupal\codev_ticket_system\Entity\Ticket $ticket */
      $ticket = $build[$child]['#ticket'];
      $bundle = $ticket->getBundleEntity();
      if ($bundle && $bundle->getApi() && $ticket->isApiEnabled()
        && $id = Field::fmtCleanValue('api_id', $ticket)) {
        $api_data[$bundle->id()]['bundle'] = $bundle;
        $api_data[$bundle->id()]['ids'][$ticket->id()] = $id;
      }
    }

    foreach ($api_data as $data) {
      /** @var \Drupal\codev_ticket_system\Entity\TicketType $bundle */
      $bundle = $data['bundle'];
      $ids = array_unique($data['ids'] ?? []);
      $tasks[$bundle->id()] = $bundle->getApi()->loadTasksByIds($ids);
    }

    foreach ($child_arr as $child) {
      /** @var \Drupal\codev_ticket_system\Entity\Ticket $ticket */
      $ticket = $build[$child]['#ticket'];
      $bundle = $ticket->getBundleEntity();
      if ($bundle && $ticket->isApiEnabled() && $api = $bundle->getApi()) {
        $id = Field::fmtCleanValue('api_id', $ticket, '');
        if ($api->sync($ticket, $tasks[$bundle->id()][$id] ?? [])) {
          try {
            $ticket->disableApi();
            $ticket->save();
          } catch (Exception $exception) {
            Drupal::logger(Settings::MODULE_NAME)
              ->warning($exception->getMessage());
          }
        }
      }
    }

    return $build;
  }

}
