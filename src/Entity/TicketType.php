<?php

namespace Drupal\codev_ticket_system\Entity;

use Drupal;
use Drupal\codev_ticket_system\Plugin\TicketSystemApiInterface;
use Drupal\codev_ticket_system\Plugin\TicketSystemApiManager;
use Drupal\codev_ticket_system\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\user\Entity\User;
use Exception;

/**
 * Defines the Ticket type entity.
 *
 * @ConfigEntityType(
 *   id = "ticket_type",
 *   label = @Translation("Ticket type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\codev_ticket_system\TicketTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\codev_ticket_system\Form\TicketTypeForm",
 *       "edit" = "Drupal\codev_ticket_system\Form\TicketTypeForm",
 *       "delete" = "Drupal\codev_ticket_system\Form\TicketTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "ticket_type",
 *   admin_permission = "administer ticket system",
 *   bundle_of = "ticket",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "notification",
 *     "notification_from",
 *     "notification_to",
 *     "notification_reply",
 *     "information_helper",
 *     "api_data"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ticket_type/{ticket_type}",
 *     "add-form" = "/admin/structure/ticket_type/add",
 *     "edit-form" = "/admin/structure/ticket_type/{ticket_type}/edit",
 *     "delete-form" = "/admin/structure/ticket_type/{ticket_type}/delete",
 *     "collection" = "/admin/structure/ticket_types"
 *   }
 * )
 *
 * @noinspection AnnotationMissingUseInspection
 *
 * @noinspection PhpUnused
 */
class TicketType extends ConfigEntityBundleBase implements TicketTypeInterface {

  /**
   * The Ticket type ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The Ticket type label.
   *
   * @var string
   */
  protected string $label;

  /**
   * A brief description of this ticket type.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup|string
   */
  protected $description = '';

  /**
   * Notification status.
   *
   * @var bool
   */
  protected bool $notification = TRUE;

  /**
   * Notification from address.
   *
   * @var string
   */
  protected string $notification_from = '';

  /**
   * Notification to address.
   *
   * @var array
   */
  protected array $notification_to = [];

  /**
   * Notification reply address.
   *
   * @var array
   */
  protected array $notification_reply = [];

  /**
   * Information helper list.
   *
   * @var array
   */
  protected array $information_helper = [];

  /**
   * API data.
   *
   * @var array
   */
  protected array $api_data = [];

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description): TicketTypeInterface {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isNotificationEnable(): bool {
    $settings_key = [
      'config',
      'codev_ticket_system.settings',
      'notification',
      'enable',
    ];
    $global_notification = Settings::get('notification')['enable'] ?? TRUE;
    if (NestedArray::keyExists($GLOBALS, $settings_key)) {
      $global_notification = NestedArray::getValue($GLOBALS, $settings_key);
    }
    if ($global_notification) {
      return $this->notification;
    }
    return $global_notification;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotification(bool $notification): TicketTypeInterface {
    $this->notification = $notification;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationFrom(): string {
    if (empty($this->notification_from)) {
      $site_mail = $this->getSiteSettings()['mail'] ?? '';
      $notificationSettings = $this->getNotificationSettings();
      return Utils::getArrayValue('from', $notificationSettings, $site_mail);
    }
    return $this->notification_from;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationFrom(string $notification_from): TicketTypeInterface {
    $this->notification_from = $notification_from;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationTo(): array {
    if (empty($this->notification_to)) {
      $site_mail = Utils::getArrayValue('mail', $this->getSiteSettings());
      return Utils::getArrayValue(
        'to', $this->getNotificationSettings(),
        !empty($site_mail) ? [$site_mail] : []
      );
    }
    return $this->notification_to;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationTo(array $notification_to): TicketTypeInterface {
    $this->notification_to = $notification_to;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationReply(): array {
    if (empty($this->notification_reply)) {
      $admin_mail = $this->getAdminMail();
      $admin_mail_arr = !empty($admin_mail) ? [$admin_mail] : [];
      $notificationSettings = $this->getNotificationSettings();
      return Utils::getArrayValue('reply', $notificationSettings, $admin_mail_arr);
    }
    return $this->notification_reply;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationReply(array $notification_reply): TicketTypeInterface {
    $this->notification_reply = $notification_reply;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getInformationHelper(): array {
    return $this->information_helper;
  }

  /**
   * {@inheritdoc}
   */
  public function setInformationHelper(array $information_helper): TicketTypeInterface {
    $this->information_helper = $information_helper;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isSetApiUsage(): bool {
    return !empty(Utils::getArrayValue('api', $this->getApiData()));
  }

  /**
   * {@inheritdoc}
   */
  public function getApi(): ?TicketSystemApiInterface {
    if ($this->isSetApiUsage()) {
      try {
        /** @var TicketSystemApiManager $api_manager */
        $api_manager = Drupal::service('plugin.manager.ticket_system_api');
        $config = $this->getApiData();
        $api = Utils::getArrayValue('api', $config, '');
        $unset = ['name', 'api'];
        foreach ($config as $key => $val) {
          if (in_array($key, $unset)) {
            unset($config[$key]);
          }
        }
        return $api_manager->createInstance($api, $config);
      } catch (Exception $e) {
        return NULL;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiData(): array {
    $name = $this->api_data['name'] ?? '';
    if (!empty($name) && $data = Settings::getAccessDataByName($name)) {
      $this->api_data = array_merge($this->api_data, $data);
    }
    return $this->api_data;
  }

  /**
   * {@inheritdoc}
   */
  public function setApiData(array $api_data): TicketTypeInterface {
    $this->api_data = $api_data;
    return $this;
  }

  /**
   * Return the current site settings.
   *
   * @return array
   */
  private function getSiteSettings(): array {
    return Drupal::config('system.site')->getRawData() ?: [];
  }

  /**
   * Return the current notification settings.
   *
   * @return array
   */
  private function getNotificationSettings(): array {
    return Settings::get('notification') ?: [];
  }

  /**
   * Return the admin mail.
   *
   * @return string
   */
  private function getAdminMail(): ?string {
    try {
      $user = Drupal::entityTypeManager()->getStorage('user')->load(1);
      if ($user instanceof User) {
        return $user->getEmail();
      }
      return NULL;
    } catch (Exception $e) {
      return NULL;
    }
  }

}
