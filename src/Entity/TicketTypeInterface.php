<?php

namespace Drupal\codev_ticket_system\Entity;

use Drupal\codev_ticket_system\Plugin\TicketSystemApiInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Ticket type entities.
 *
 * @noinspection PhpUnused
 */
interface TicketTypeInterface extends ConfigEntityInterface {

  /**
   * Get the ticket type description.
   *
   * @return string
   */
  public function getDescription(): string;

  /**
   * Set the ticket type description.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $description
   *
   * @return $this
   */
  public function setDescription($description): TicketTypeInterface;

  /**
   * Return the notification status.
   *
   * @return bool
   */
  public function isNotificationEnable(): bool;

  /**
   * Set the notification status.
   *
   * @param bool $notification
   *
   * @return $this
   */
  public function setNotification(bool $notification): TicketTypeInterface;

  /**
   * Get the ticket type notification from address.
   *
   * @return string
   */
  public function getNotificationFrom(): string;

  /**
   * Set the ticket type notification from address.
   *
   * @param string $notification_from
   *
   * @return $this
   */
  public function setNotificationFrom(string $notification_from): TicketTypeInterface;

  /**
   * Get the ticket type notification to address.
   *
   * @return array
   */
  public function getNotificationTo(): array;

  /**
   * Set the ticket type notification to address.
   *
   * @param array $notification_to
   *
   * @return $this
   */
  public function setNotificationTo(array $notification_to): TicketTypeInterface;

  /**
   * Get the ticket type notification reply address.
   *
   * @return array
   */
  public function getNotificationReply(): array;

  /**
   * Set the ticket type notification reply address.
   *
   * @param array $notification_reply
   *
   * @return $this
   */
  public function setNotificationReply(array $notification_reply): TicketTypeInterface;

  /**
   * Get the ticket type information helper data.
   *
   * @return array
   */
  public function getInformationHelper(): array;

  /**
   * Set the ticket type information helper data.
   *
   * @param array $information_helper
   *
   * @return $this
   */
  public function setInformationHelper(array $information_helper): TicketTypeInterface;

  /**
   * Return true if is set an api data to use for connections.
   *
   * @return bool
   */
  public function isSetApiUsage(): bool;

  /**
   * Get the ticket system api manager service.
   *
   * @return \Drupal\codev_ticket_system\Plugin\TicketSystemApiInterface
   */
  public function getApi(): ?TicketSystemApiInterface;

  /**
   * Get the ticket type api data.
   *
   * @return array
   */
  public function getApiData(): array;

  /**
   * Set the ticket type api data.
   *
   * @param array $api_data
   *
   * @return $this
   */
  public function setApiData(array $api_data): TicketTypeInterface;

}
