<?php

namespace Drupal\codev_ticket_system\Entity;

use Drupal;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Ticket entity.
 *
 * @ingroup      codev_ticket_system
 *
 * @ContentEntityType(
 *   id = "ticket",
 *   label = @Translation("Ticket"),
 *   bundle_label = @Translation("Ticket type"),
 *   handlers = {
 *     "view_builder" = "Drupal\codev_ticket_system\Entity\TicketViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\codev_ticket_system\Form\TicketForm",
 *       "add" = "Drupal\codev_ticket_system\Form\TicketForm",
 *       "edit" = "Drupal\codev_ticket_system\Form\TicketForm",
 *       "delete" = "Drupal\codev_ticket_system\Form\TicketDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\codev_ticket_system\TicketHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\codev_ticket_system\TicketAccessControlHandler",
 *   },
 *   base_table = "ticket",
 *   data_table = "ticket_field_data",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer ticket entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "published",
 *   },
 *   links = {
 *     "canonical" = "/admin/ticket/{ticket}",
 *     "add-page" = "/admin/ticket/add",
 *     "add-form" = "/admin/ticket/add/{ticket_type}",
 *     "edit-form" = "/admin/ticket/{ticket}/edit",
 *     "delete-form" = "/admin/ticket/{ticket}/delete"
 *   },
 *   bundle_entity_type = "ticket_type",
 *   field_ui_base_route = "entity.ticket_type.edit_form"
 * )
 *
 * @noinspection AnnotationMissingUseInspection
 *
 * @noinspection PhpUnused
 */
class Ticket extends ContentEntityBase implements TicketInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * Enable/Disable API connection for current entity
   */
  private bool $apiEnabled = TRUE;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += ['user_id' => Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Create by'))
      ->setDescription(t('The user ID of creator of the Ticket.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label'  => 'inline',
        'type'   => 'author',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 1,
        'settings' => [
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Ticket.'))
      ->setSettings(['max_length' => 512])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label'  => 'hidden',
        'type'   => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type'   => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of the Ticket.'))
      ->setSettings(['max_length' => 128])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label'  => 'inline',
        'type'   => 'string',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type'   => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['importance'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Importance'))
      ->setDescription(t('The importance of the Ticket.'))
      ->setSettings(['max_length' => 128])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label'  => 'inline',
        'type'   => 'string',
        'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'type'   => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('The description of the Ticket.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label'  => 'above',
        'type'   => 'text_default',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'type'   => 'text_textarea',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['attachments'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Attachments'))
      ->setDescription(t('Additional attachments of files for the ticket.'))
      ->setSettings([
        'uri_scheme'      => 'public',
        'max_filesize'    => '500 MB',
        'file_extensions' => 'jpg jpeg png gif txt doc docx pdf xml xls',
      ])
      ->setDisplayOptions('view', [
        'label'  => 'hidden',
        'type'   => 'file_table',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type'   => 'file_generic',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $fields['comments'] = BaseFieldDefinition::create('comment')
      ->setLabel(t('Comments'))
      ->setDescription(t('Add new comment'))
      ->setDefaultValue(['status' => 2])
      ->setSettings([
        'per_page'     => 50,
        'preview'      => 0,
        'comment_type' => 'ticket',
      ])
      ->setDisplayOptions('view', [
        'label'  => 'hidden',
        'type'   => 'comment_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['published']->setDescription(t('Published status of the Ticket.'))
      ->setDisplayOptions('form', [
        'type'   => 'boolean_checkbox',
        'weight' => 99,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the Ticket was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the Ticket was last edited.'));

    $fields['api_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('API id'))
      ->setDescription(t('Mapping id form API handling.'))
      ->setSettings(['max_length' => 255])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['api_attachment_ids'] = BaseFieldDefinition::create('string')
      ->setLabel(t('API attachment ids'))
      ->setDescription(t('Mapping ids form API attachments handling.'))
      ->setSettings(['max_length' => 255])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $fields['api_comment_ids'] = BaseFieldDefinition::create('string')
      ->setLabel(t('API comment ids'))
      ->setDescription(t('Mapping ids form API comments handling.'))
      ->setSettings(['max_length' => 255])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $fields['api_data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('API data'))
      ->setDescription(t('API data hash'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): TicketInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): TicketInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): ?int {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): TicketInterface {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): TicketInterface {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleEntity(): ?TicketTypeInterface {
    $referencedEntities = $this->referencedEntities();
    foreach ($referencedEntities as $entity) {
      if ($entity instanceof TicketTypeInterface) {
        return $entity;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isApiEnabled(): bool {
    return $this->apiEnabled;
  }

  /**
   * {@inheritdoc}
   */
  public function disableApi(bool $disable = TRUE): TicketInterface {
    $this->apiEnabled = !$disable;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function save(): int {
    $bundle_entity = $this->getBundleEntity();
    $api_enable = $this->isApiEnabled() && $bundle_entity;
    if ($api_enable && $api = $bundle_entity->getApi()) {
      if ($this->isNew()) {
        $api->create($this);
      }
      else {
        $api->update($this);
      }
    }
    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    parent::delete();
    $bundle_entity = $this->getBundleEntity();
    $api_enable = $this->isApiEnabled() && $bundle_entity;
    if ($api_enable && $api = $bundle_entity->getApi()) {
      $api->delete($this);
    }
  }

}
