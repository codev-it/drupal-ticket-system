<?php

namespace Drupal\codev_ticket_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ticket entities.
 *
 * @ingroup      codev_ticket_system
 *
 * @noinspection PhpUnused
 */
interface TicketInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Gets the Ticket name.
   *
   * @return string
   *   Name of the Ticket.
   */
  public function getName(): string;

  /**
   * Sets the Ticket name.
   *
   * @param string $name
   *   The Ticket names.
   *
   * @return \Drupal\codev_ticket_system\Entity\TicketInterface
   *   The called Ticket entity.
   */
  public function setName(string $name): TicketInterface;

  /**
   * Gets the Ticket creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ticket.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Ticket creation timestamp.
   *
   * @param int $timestamp
   *   The Ticket creation timestamp.
   *
   * @return \Drupal\codev_ticket_system\Entity\TicketInterface
   *   The called Ticket entity.
   */
  public function setCreatedTime(int $timestamp): TicketInterface;

  /**
   * Gets the Ticket bundle entity.
   *
   * @return \Drupal\codev_ticket_system\Entity\TicketTypeInterface|null
   *   Ticket bundle entity.
   */
  public function getBundleEntity(): ?TicketTypeInterface;

  /**
   * Return api status.
   *
   * @return bool
   */
  public function isApiEnabled(): bool;

  /**
   * Set disable status for the api connection.
   *
   * @param bool $disable
   *
   * @return \Drupal\codev_ticket_system\Entity\TicketInterface
   *   The called Ticket entity.
   */
  public function disableApi(bool $disable): TicketInterface;

}
