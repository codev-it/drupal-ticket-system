<?php

namespace Drupal\codev_ticket_system;

use Drupal\codev_ticket_system\Entity\TicketType;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides dynamic permissions for Ticket of different types.
 *
 * @ingroup      codev_ticket_system
 *
 * @noinspection PhpUnused
 */
class TicketPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The Ticket by bundle permissions.
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions(): array {
    $perms = [];
    foreach (TicketType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }
    return $perms;
  }

  /**
   * Returns a list of ticket permissions for a given ticket type.
   *
   * @param \Drupal\codev_ticket_system\Entity\TicketType $type
   *   The Ticket types.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(TicketType $type): array {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      'add ticket entities ' . $type_id              => [
        'title' => $this->t('Create new %type_name ticket entities', $type_params),
      ],
      'edit own ticket entities ' . $type_id         => [
        'title' => $this->t('Edit own %type_name ticket entities', $type_params),
      ],
      'edit any ticket entities ' . $type_id         => [
        'title' => $this->t('Edit any %type_name ticket entities', $type_params),
      ],
      'delete own ticket entities ' . $type_id       => [
        'title' => $this->t('Delete own %type_name ticket entities', $type_params),
      ],
      'delete any ticket entities ' . $type_id       => [
        'title' => $this->t('Delete any %type_name ticket entities', $type_params),
      ],
      'view published ticket entities ' . $type_id   => [
        'title' => $this->t('View published %type_name ticket entities', $type_params),
      ],
      'view unpublished ticket entities ' . $type_id => [
        'title' => $this->t('View unpublished %type_name ticket entities', $type_params),
      ],
    ];
  }

}
