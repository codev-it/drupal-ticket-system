<?php

namespace Drupal\codev_ticket_system;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Ticket entity.
 *
 * @see          \Drupal\codev_ticket_system\Entity\Ticket.
 *
 * @noinspection PhpUnused
 */
class TicketAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $permission = ['add ticket entities'];
    if ($entity_bundle) {
      $permission[] = sprintf('%s %s', $permission[0], $entity_bundle);
    }
    return AccessResult::allowedIfHasPermissions($account, $permission, 'OR');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\codev_ticket_system\Entity\TicketInterface $entity */

    switch ($operation) {
      case 'update':
        if (!empty($this->checkOwn($entity, $operation, $account))) {
          return AccessResult::allowed();
        }
        $permission = 'edit any ticket entities';
        return AccessResult::allowedIfHasPermissions($account, [
          $permission,
          sprintf('%s %s', $permission, $entity->bundle()),
        ], 'OR');

      case 'delete':
        if (!empty($this->checkOwn($entity, $operation, $account))) {
          return AccessResult::allowed();
        }
        $permission = 'delete any ticket entities';
        return AccessResult::allowedIfHasPermissions($account, [
          $permission,
          sprintf('%s %s', $permission, $entity->bundle()),
        ], 'OR');

      case 'view':
        $permission = 'view published ticket entities';
        if (!$entity->isPublished()) {
          $permission = 'view unpublished ticket entities';
        }
        return AccessResult::allowedIfHasPermissions($account, [
          $permission,
          sprintf('%s %s', $permission, $entity->bundle()),
        ], 'OR');
    }

    return AccessResult::neutral();
  }

  /**
   * Test for given 'own' permission.
   *
   * @param \Drupal\Core\Entity\EntityInterface   $entity
   * @param                                       $operation
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return string|null
   *   The permission string indicating it's allowed.
   */
  protected function checkOwn(EntityInterface $entity, $operation, AccountInterface $account): ?string {
    /** @var \Drupal\codev_ticket_system\Entity\TicketInterface $entity */
    $uid = $entity->getOwnerId();
    $is_own = $account->isAuthenticated() && $account->id() == $uid;
    $ops = [
      'update' => 'edit own ticket entities',
      'delete' => 'delete own ticket entities',
    ];
    $permission = $ops[$operation];
    $permission_type = sprintf('%s %s', $permission, $entity->bundle());

    if (!$is_own) {
      return NULL;
    }

    if ($account->hasPermission($permission)) {
      return $permission;
    }

    if ($account->hasPermission($permission_type)) {
      return $permission_type;
    }

    return NULL;
  }

}
