<?php


namespace Drupal\codev_ticket_system\Controller;


use Drupal;
use Drupal\codev_ticket_system\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Entity\Controller\EntityController;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: TicketEntityController.php
 * .
 */

/**
 * Class TicketEntityController.
 *
 * @package      Drupal\codev_ticket_system\Controller
 *
 * @noinspection PhpUnused
 */
class TicketEntityController extends EntityController {

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function addPage($entity_type_id) {
    $build = parent::addPage($entity_type_id);
    $build['#theme'] = 'ticket_content_add_list';
    $modal_settings = Settings::get('entity_modal');
    $isXmlRequest = Drupal::request()->isXmlHttpRequest();
    /** @var \Drupal\codev_ticket_system\Entity\TicketType[] $bundles */
    $bundles = $this->entityTypeManager
      ->getStorage('ticket_type')
      ->loadMultiple();
    foreach ($build['#bundles'] ?? [] as $bundle => $item) {
      if ($bundle_info = $bundles[$bundle] ?? NULL) {
        $build['#bundles'][$bundle]['description'] = $bundle_info->getDescription();
        if ($isXmlRequest && Utils::getArrayValue('enable', $modal_settings, FALSE)) {
          /** @var \Drupal\Core\Link $link */
          $link = $item['add_link'];
          $url = $link->getUrl();
          $attr = $url->getOption('attributes') ?? [];
          $width = Utils::getArrayValue('width', $modal_settings, FALSE);
          Utils::appendDialogModalAttr($attr, $width ? ['width' => $width] : []);
          $url->setOption('attributes', $attr);
        }
      }
    }
    return $build;
  }

}
