<?php

namespace Drupal\codev_ticket_system\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a Ticket system api item annotation object.
 *
 * @see \Drupal\codev_ticket_system\Plugin\TicketSystemApiManager
 * @see plugin_api
 *
 * @Annotation
 *
 * @noinspection PhpUnused
 */
class TicketSystemApi extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The form field for the access data widget in the settings form.
   *
   * @var array
   */
  public array $access_data_fields = [];

}
