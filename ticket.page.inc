<?php

/**
 * @file
 * Contains ticket.page.inc.
 *
 * Page callback for Ticket entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Ticket templates.
 *
 * Default template: ticket.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 *
 * @noinspection PhpUnused
 */
function template_preprocess_ticket(array &$variables) {
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Prepares variables for Ticket notification templates.
 *
 * Default template: ticket-notification.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 *
 * @throws \Drupal\Core\Entity\EntityMalformedException
 *
 * @noinspection PhpUnused
 */
function template_preprocess_ticket_notification(array &$variables) {
  /** @var \Drupal\codev_ticket_system\Entity\Ticket $ticket */
  $ticket = $variables['elements']['#ticket'];
  $variables['base_url'] = Drupal::request()->headers->get('origin');
  $variables['ticket_url'] = $ticket->toUrl()->setAbsolute();
  foreach (Element::children($variables['elements']) as $key) {
    $elem = $variables['elements'][$key];
    if (!empty($elem['#field_type']) && $elem['#field_type'] == 'comment') {
      foreach (Element::children($elem) as $elem_key) {
        if (!empty($elem[$elem_key]['comment_form'])) {
          $elem[$elem_key]['comment_form'] = [];
        }
      }
    }
    $variables['content'][$key] = $elem;
  }
}
