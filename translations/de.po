# German translation of codev_ticket_system module.
#
# Copyright (c) 2018 by codev-it
#
msgid ""
msgstr ""
"Project-Id-Version: codev_ticket_system\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"Last-Translator: \n"
"Language: de\n"

msgid "Ticket"
msgstr "Ticket"

msgid "Ticket system"
msgstr "Ticket-System"

msgid "Ticket system, management task and tickets."
msgstr "Ticketsystem, Verwaltung von Aufgaben und Tickets."

msgid "Task and ticket system functions, with connection to other task systems."
msgstr "Funktionen des Aufgaben- und Ticket-System, mit Anbindung an andere Aufgabensysteme."

msgid "Global settings for the Codev-IT Ticket System module"
msgstr "Globale Einstellungen für das Ticket-System-Modul von Codev-IT"

msgid "Administer ticket system"
msgstr "Verwalten des Ticket-System"

msgid "Allow to access the administration of the ticket system module and entity types."
msgstr "Ermöglicht den Zugriff auf die Verwaltung des Ticketsystemmoduls und der Entitäten-Typen."

msgid "Codev-IT Ticket System Settings"
msgstr "Codev-IT Ticket System Einstellungen"

msgid "Global API access data"
msgstr "Globale API-Zugangsdaten"

msgid "These API data are global and can be reused in all ticket type entities."
msgstr "Diese Angeben API Daten sind global und könne in allen Tickettyp Entitäten wider verwendet werden."

msgid "Add access data"
msgstr "Zugangsdaten hinzufügen"

msgid "Remove access data"
msgstr "Zugangsdaten löschen"

msgid "API access data: @name"
msgstr "API Zugangsdaten: @name"

msgid "Choose the API type"
msgstr "Wählen sie die API Typ"

msgid "API Name"
msgstr "API Name"

msgid "Administer ticket entities"
msgstr "Ticket-Entitäten verwalten"

msgid "Allow to access the administration form to configure ticket entities."
msgstr "Ermöglicht den Zugriff auf das Verwaltungsformular zur Konfiguration von Ticket-Entitäten."

msgid "Create new ticket entities"
msgstr "Neue Ticket-Entitäten erstellen"

msgid "Create new %type_name ticket entities"
msgstr "Neue %type_name Ticket-Entitäten erstellen"

msgid "Edit own ticket entities"
msgstr "Eigene Ticket-Entitäten bearbeiten"

msgid "Edit any ticket entities"
msgstr "Beliebigen Ticket-Entitäten bearbeiten"

msgid "Edit own %type_name ticket entities"
msgstr "Eigene %type_name Ticket-Entitäten bearbeiten"

msgid "Edit any %type_name ticket entities"
msgstr "Beliebigen %type_name Ticket-Entitäten bearbeiten"

msgid "Delete own ticket entities"
msgstr "Eigene Ticket-Entitäten löschen"

msgid "Delete any ticket entities"
msgstr "Beliebigen Ticket-Entitäten löschen"

msgid "Delete own %type_name ticket entities"
msgstr "Eigene %type_name Ticket-Entitäten löschen"

msgid "Delete any %type_name ticket entities"
msgstr "Beliebigen %type_name Ticket-Entitäten löschen"

msgid "View published ticket entities"
msgstr "Ansicht der veröffentlichten Ticket-Entitäten"

msgid "View unpublished ticket entities"
msgstr "Unveröffentlichte Ticket-Entitäten anzeigen"

msgid "View published %type_name ticket entities"
msgstr "Ansicht der veröffentlichten %type_name Ticket-Entitäten"

msgid "View unpublished %type_name ticket entities"
msgstr "Unveröffentlichte %type_name Ticket-Entitäten anzeigen"

msgid "Tickets"
msgstr "Tickets"

msgid "List ticket entities"
msgstr "Liste Ticket-Entitäten"

msgid "Add ticket"
msgstr "Ticket hinzufügen"

msgid "Ticket type"
msgstr "Tickettyp"

msgid "Ticket types"
msgstr "Tickettypen"

msgid "List ticket type (bundles)"
msgstr "Liste Tickettypen (Bündel)"

msgid "Add ticket type"
msgstr "Tickettyp hinzufügen"

msgid "Add new ticket type (bundle)"
msgstr "Neue Tickettyp hinzufügen (Bündel)"

msgid "Are you sure you want to delete ticket type %name?"
msgstr "Sind Sie sicher, dass Sie den Tickettyp %name löschen möchten?"

msgid "Ticket type @label deleted."
msgstr "Tickettyp @label gelöscht."

msgid "API"
msgstr "API"

msgid "Label"
msgstr "Beschriftung"

msgid "Label for the Ticket type."
msgstr "Beschriftung für den Tickettyp."

msgid "Description for the Ticket type."
msgstr "Beschreibung für den Tickettyp."

msgid "Notification"
msgstr "Benachrichtigung"

msgid "Enable/Disable E-Mail notification."
msgstr "Aktivieren/Deaktivieren der E-Mail-Benachrichtigung."

msgid "Select the API to connect with the ticket type."
msgstr "Wählen Sie die API für die Verbindung mit dem Ticket-Typ."

msgid "None"
msgstr "Keine"

msgid "-- None --"
msgstr "-- Keine --"

msgid "Custom"
msgstr "Benutzerdefiniert"

msgid "Yes"
msgstr "Ja"

msgid "No"
msgstr "Nein"

msgid "Created the %label Ticket type."
msgstr "Der Tickettyp %label wurde erstellt."

msgid "Saved the %label Ticket type."
msgstr "Die Tickettyp %label wurde gespeichert."

msgid "No tickets available."
msgstr "Keine Tickets verfügbar"

msgid "Default"
msgstr "Standard"

msgid "Standard ticket this can be used for all unspecified tickets."
msgstr "Standard Ticket, dies kann für alle nicht spezifizierten Tickets genutzt werden."

msgid "Bug"
msgstr "Fehler"

msgid "If you find an bug, you can report it here directly."
msgstr "Sollte sie einen Fehler finden können sie diese hiermit direkt melden."

msgid "Report bug"
msgstr "Fehler melden"

msgid "Create fast bug report"
msgstr "Schnellen Fehlerbericht erstellen"

msgid "Create by"
msgstr "Erstellt von"

msgid "Ticket entity view settings"
msgstr "Einstellungen der Ticket-Entität-Ansicht"

msgid "Open entities in a modal window"
msgstr "Ticket-Entitäten in einem modalen Fenster öffnen"

msgid "Modal window size"
msgstr "Größe des modalen Fensters"

msgid "The user ID of creator of the Ticket."
msgstr "Die Benutzer-ID des Erstellers der Ticket."

msgid "The name of the Ticket."
msgstr "Der Name der Ticket."

msgid "Published status of the Ticket."
msgstr "Veröffentlichter Status des Tickets."

msgid "The time that the Ticket was created."
msgstr "Die Zeit, zu der das Ticket erstellt wurde."

msgid "The time that the Ticket was last edited."
msgstr "Die Zeit, zu der das Ticket zuletzt bearbeitet wurde."

msgid "Created the %label Ticket."
msgstr "Das Ticket, %label wurde erstellt."

msgid "Saved the %label Ticket."
msgstr "Das Ticket, %label wurde gespeichert."

msgid "The description of the Ticket."
msgstr "Die Beschreibung des Tickets."

msgid "Importance"
msgstr "Wichtigkeit"

msgid "The importance of the Ticket."
msgstr "Die Wichtigkeit des Tickets."

msgid "Attachments"
msgstr "Anhänge"

msgid "Additional attachments of files for the ticket."
msgstr "Zusätzliche Anhänge von Dateien für das Ticket."

msgid "Comments"
msgstr "Kommentare"

msgid "Add new comment"
msgstr "Neuen Kommentar hinzufügen"

msgid "Status"
msgstr "Status"

msgid "The status of the Ticket."
msgstr "Der Status des Tickets."

msgid "Ticket global notification settings"
msgstr "Einstellungen für globale Ticket-Benachrichtigungen"

msgid "Globally enable/disable notifications"
msgstr "Globale Aktivierung/Deaktivierung von Benachrichtigungen"

msgid "Mail from address"
msgstr "E-Mail Absende Adresse"

msgid "Mail to addresses"
msgstr "E-Mail Versand an Adressen"

msgid "Mail reply addresses"
msgstr "E-Mail-Reply-Adressen"

msgid "Separate multiple addresses with a space or comma."
msgstr "Trennen Sie mehrere Adressen durch ein Leerzeichen oder ein Komma."

msgid "New Ticket[@ticket-id] created: @ticket-name"
msgstr "Neues Ticket[@ticket-id] erstellen: @ticket-name"

msgid "Update Ticket[@ticket-id]: @ticket-name"
msgstr "Ticket[@ticket-id] aktualisieren: @ticket-name"

msgid "Closed Ticket[@ticket-id]: @ticket-name"
msgstr "Ticket[@ticket-id] geschlossen: @ticket-name"

msgid "Deleted Ticket[@ticket-id]: @ticket-name"
msgstr "Ticket[@ticket-id] gelöscht: @ticket-name"

msgid "Select the folder you want to sync"
msgstr "Wählen Sie den Ordner aus, den Sie synchronisieren möchten"

msgid "New"
msgstr "Neu"

msgid "Active"
msgstr "Aktiv"

msgid "Completed"
msgstr "Abgeschlossen"

msgid "Deferred"
msgstr "Aufgeschoben"

msgid "Cancelled"
msgstr "Abgesagt"

msgid "High"
msgstr "Hoch"

msgid "Normal"
msgstr "Normal"

msgid "Low"
msgstr "Niedrig"

msgid "API id"
msgstr "API id"

msgid "Mapping id form API handling."
msgstr "Mapping id für die API-Behandlung."

msgid "API attachment ids"
msgstr "API Anhang ids"

msgid "Mapping ids form API attachments handling."
msgstr "Mapping id für die API-Behandlung Anhänge."

msgid "API comment ids"
msgstr "API Kommentar ids"

msgid "Mapping ids form API comments handling."
msgstr "Mapping id für die API-Behandlung Kommentare."

msgid "Default information's"
msgstr "Standardinformationen"

msgid "This information's will be provided as standard by create an ticket."
msgstr "Diese Informationen werden standardmäßig bei der Erstellung eines Tickets bereitgestellt."

msgid "Attach browser info to ticket description."
msgstr "Browser-Infos an die Ticket-Beschreibung anhängen."

msgid "Create and attach a screenshot. (Only available in modal mode.)"
msgstr "Bildschirmfoto erstellen und anhängen. (Nur im modalen Modus verfügbar.)"

msgid "Send notification mail"
msgstr "Benachrichtigungsmail senden"

msgid "Last comment"
msgstr "Letzter Kommentar"

msgid "Wrike API permanent token"
msgstr "Wrike API dauerhaftes Token"

msgid "New ticket description TranslatableMarkup"
msgstr "Neue Ticketbeschreibung TranslatableMarkup"

msgid "Ticket description"
msgstr "Ticket-Beschreibung"

msgid "API data"
msgstr "API Daten"

msgid "API data hash"
msgstr "API Daten Hash"
