<?php

namespace Drupal\codevit_ticket_system_test\Plugin\TicketSystemApi;

use Drupal\codev_ticket_system\Annotation\TicketSystemApi;
use Drupal\codev_ticket_system\Entity\Ticket;
use Drupal\codev_ticket_system\Plugin\TicketSystemApiBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_pages.module
 * .
 */

/**
 * Class TestApi.
 *
 * @package Drupal\codevit_ticket_system\Api
 *
 * @TicketSystemApi(
 *   id = "test",
 *   label = @Translation("Test API"),
 *   access_data_fields = {
 *     "Id" = "textfield",
 *     "Secret" = "textfield",
 *   }
 * )
 *
 * @noinspection PhpUnused
 */
class TestApi extends TicketSystemApiBase {

  /**
   * @inheritDoc
   */
  public function create(Ticket $ticket) { }

  /**
   * @inheritDoc
   */
  public function update(Ticket $ticket) { }

  /**
   * @inheritDoc
   */
  public function delete(Ticket $ticket) { }

  /**
   * @inheritDoc
   */
  public function sync(Ticket $ticket, array $data): bool {
    return TRUE;
  }

  /**
   * @inheritDoc
   */
  public function loadTasks(): array {
    return [];
  }

  /**
   * @inheritDoc
   */
  public function loadTasksByIds(array $ids): array {
    return [];
  }

}
