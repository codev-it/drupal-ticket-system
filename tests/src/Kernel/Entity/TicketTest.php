<?php

namespace Drupal\Tests\codev_ticket_system\Kernel\Entity;

use Drupal\codev_ticket_system\Entity\Ticket;
use Drupal\codev_ticket_system\Entity\TicketType;
use Drupal\codev_ticket_system\Entity\TicketTypeInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: TicketTest.php
 * .
 */

/**
 * Class TicketTest.
 *
 * Unit tests for the ticket class.
 *
 * @package      Drupal\Tests\codev_ticket_system\Kernel\Entity
 *
 * @group        codev_ticket_system
 *
 * @noinspection PhpUnused
 */
class TicketTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_ticket_system',
    'system',
    'text',
    'user',
    'file',
    'comment',
    'filter',
  ];

  /**
   * Entity
   *
   * @var \Drupal\codev_ticket_system\Entity\Ticket
   */
  private $entity;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'user']);
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('ticket');
    $this->installEntitySchema('ticket_type');
    $this->installEntitySchema('user');

    TicketType::create([
      'id'    => 'test',
      'label' => t('Ticket type'),
    ])->save();

    $this->entity = Ticket::create([
      'id'    => 1,
      'type'  => 'test',
      'title' => t('Ticket type'),
    ]);
    $this->entity->save();
  }

  /**
   * Test getBundleEntity.
   */
  public function testGetBundleEntity() {
    $this->assertTrue($this->entity->getBundleEntity() instanceof TicketTypeInterface);
  }

}
