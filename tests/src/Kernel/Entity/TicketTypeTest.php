<?php

namespace Drupal\Tests\codev_ticket_system\Kernel\Entity;

use Drupal\codev_ticket_system\Entity\TicketType;
use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Entity\User;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: TicketTypeTest.php
 * .
 */

/**
 * Class TicketTypeTest.
 *
 * Unit tests for the ticket type class.
 *
 * @package      Drupal\Tests\codev_ticket_system\Kernel\Entity
 *
 * @group        codev_ticket_system
 *
 * @noinspection PhpUnused
 */
class TicketTypeTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_ticket_system',
    'system',
    'text',
    'user',
  ];

  /**
   * Entity type
   *
   * @var \Drupal\codev_ticket_system\Entity\TicketType
   */
  private $entityType;

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $siteSettings;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $adminSettings;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'user']);
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('ticket_type');
    $this->installEntitySchema('user');

    $this->entityType = TicketType::create([
      'id'          => 1,
      'label'       => t('Ticket type'),
      'description' => t('Ticket description'),
    ]);
    $this->entityType->save();

    $this->configFactory = $this->container->get('config.factory');
    $this->siteSettings = $this->configFactory->getEditable('system.site');
    $this->adminSettings = $this->configFactory->getEditable('codev_ticket_system.settings');
    $this->siteSettings->setData([])->save();
    $this->adminSettings->setData([])->save();
  }

  /**
   * Check entity type id.
   */
  public function testId() {
    $this->assertEquals(1, $this->entityType->id());
  }

  /**
   * Check entity type label.
   */
  public function testLabel() {
    $this->assertEquals(t('Ticket type'), $this->entityType->label());
  }

  /**
   * Check entity type description.
   */
  public function testDescription() {
    $this->assertEquals(t('Ticket description'), $this->entityType->getDescription());

    $this->entityType->setDescription('New ticket description string');
    $this->assertEquals('New ticket description string', $this->entityType->getDescription());

    $this->entityType->setDescription(t('New ticket description TranslatableMarkup'));
    $this->assertEquals(t('New ticket description TranslatableMarkup'), $this->entityType->getDescription());
  }

  /**
   * Check entity type notification.
   */
  public function testNotification() {
    $this->assertTrue($this->entityType->isNotificationEnable());

    $this->entityType->setNotification(FALSE);
    $this->assertFalse($this->entityType->isNotificationEnable());
  }

  /**
   * Check entity type notification from.
   */
  public function testNotificationFrom() {
    $this->assertEquals('', $this->entityType->getNotificationFrom());

    $this->siteSettings->set('mail', 'siteFrom@mail.at')->save();
    $this->assertEquals('siteFrom@mail.at', $this->entityType->getNotificationFrom());

    $this->adminSettings->set('notification', ['from' => 'adminFrom@mail.at'])
      ->save();
    $this->assertEquals('adminFrom@mail.at', $this->entityType->getNotificationFrom());

    $this->entityType->setNotificationFrom('entityTypeFrom@mail.at');
    $this->assertEquals('entityTypeFrom@mail.at', $this->entityType->getNotificationFrom());
  }

  /**
   * Check entity type notification to.
   */
  public function testNotificationTo() {
    $this->assertEquals([], $this->entityType->getNotificationTo());

    $this->siteSettings->set('mail', 'siteTo@mail.at')->save();
    $this->assertEquals(['siteTo@mail.at'], $this->entityType->getNotificationTo());

    $this->adminSettings->set('notification', ['to' => ['adminTo@mail.at']])
      ->save();
    $this->assertEquals(['adminTo@mail.at'], $this->entityType->getNotificationTo());

    $this->entityType->setNotificationTo(['entityTypeTo@mail.at']);
    $this->assertEquals(['entityTypeTo@mail.at'], $this->entityType->getNotificationTo());
  }

  /**
   * Check entity type notification to.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testNotificationReply() {
    $this->assertEquals([], $this->entityType->getNotificationReply());

    User::create([
      'id'   => 1,
      'name' => 'Admin',
      'mail' => 'superadmin@mail.at',
    ])->save();
    $this->assertEquals(['superadmin@mail.at'], $this->entityType->getNotificationReply());

    $this->adminSettings->set('notification', ['reply' => ['adminReply@mail.at']])
      ->save();
    $this->assertEquals(['adminReply@mail.at'], $this->entityType->getNotificationReply());

    $this->entityType->setNotificationReply(['entityTypeReply@mail.at']);
    $this->assertEquals(['entityTypeReply@mail.at'], $this->entityType->getNotificationReply());
  }

}
