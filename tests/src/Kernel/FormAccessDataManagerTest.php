<?php

namespace Drupal\Tests\codev_ticket_system\Kernel;

use Drupal\codev_ticket_system\FormAccessDataManager;
use Drupal\KernelTests\KernelTestBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: FormAccessDataManagerTest.php
 * .
 */

/**
 * Class SettingsTest.
 *
 * Unit tests for the settings class.
 *
 * @package      Drupal\Tests\codev_ticket_system\Kernel
 *
 * @group        codev_ticket_system
 *
 * @noinspection PhpUnused
 */
class FormAccessDataManagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_ticket_system',
    'codev_ticket_system_test',
  ];

  /**
   * Test: Settings::buildSelectOptions().
   */
  public function testBuildSelectOptions() {
    $options = FormAccessDataManager::buildSelectOptions();
    $this->assertArrayHasKey('test', $options);
    $this->assertEquals(t('Test API'), $options['test']);
  }

  /**
   * Test: Settings::buildSelectOptions().
   */
  public function testBuildWidget() {
    $options = FormAccessDataManager::buildWidget();
    $this->assertArrayHasKey('api_select', $options);
    $this->assertArrayHasKey('test', $options);
    $this->assertArrayHasKey('api_name', $options['test']);
    $this->assertArrayHasKey('api_id', $options['test']);
    $this->assertArrayHasKey('api_secret', $options['test']);
  }

}
