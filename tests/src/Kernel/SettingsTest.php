<?php

namespace Drupal\Tests\codev_ticket_system\Kernel;

use Drupal\codev_ticket_system\Settings;
use Drupal\KernelTests\KernelTestBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: SettingsTest.php
 * .
 */

/**
 * Class SettingsTest.
 *
 * Unit tests for the settings class.
 *
 * @package      Drupal\Tests\codev_ticket_system\Kernel
 *
 * @group        codev_ticket_system
 *
 * @noinspection PhpUnused
 */
class SettingsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_ticket_system',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    'codev_ticket_system.settings',
  ];

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configFactory = $this->container->get('config.factory');
    $settings = $this->configFactory->getEditable('codev_ticket_system.settings');
    $settings->set('api_access_data', [
      [
        'api'           => 'api',
        'name'          => 'API 1',
        'client_id'     => 'client id 1',
        'client_secret' => 'client secret 1',
      ],
      [
        'api'           => 'api',
        'name'          => 'API 2',
        'client_id'     => 'client id 2',
        'client_secret' => 'client secret 2',
      ],
    ]);
    $settings->save();
  }

  /**
   * Test: Settings::cleanIdentifier().
   */
  public function testCleanIdentifier() {
    $this->assertEquals('', Settings::cleanIdentifier());
    $this->assertEquals('a_b__c', Settings::cleanIdentifier('A b [c]'));
  }

  /**
   * Test: Settings::getAccessDataByName().
   */
  public function testGetAccessDataByName() {
    $this->assertEquals([
      'api'           => 'api',
      'name'          => 'API 1',
      'client_id'     => 'client id 1',
      'client_secret' => 'client secret 1',
    ], Settings::getAccessDataByName('API 1'));

    $this->assertEquals([
      'api'           => 'api',
      'name'          => 'API 2',
      'client_id'     => 'client id 2',
      'client_secret' => 'client secret 2',
    ], Settings::getAccessDataByName('API 2'));
  }

}
