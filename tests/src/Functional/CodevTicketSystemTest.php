<?php

namespace Drupal\Tests\codev_ticket_system\Functional;

use Drupal\codev_ticket_system\Entity\Ticket;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: CodevTicketSystemTest.php
 * .
 */

/**
 * Class CodevTicketSystemTest.
 *
 * Tests installation module expectations.
 *
 * @package      Drupal\Tests\codev_ticket_system\Functional
 *
 * @group        codev_ticket_system
 *
 * @noinspection PhpUnused
 */
class CodevTicketSystemTest extends FunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_ticket_system',
  ];

  /**
   * Tests the functionality.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testCodevTicketSystem() {
    // Check admin form and field exist.
    $this->drupalLogin($this->drupalCreateUser(['administer ticket system']));
    $this->drupalGet('admin/config/system/codev_ticket_system');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('API data'));
    $this->assertSession()->buttonNotExists(t('Remove access data'));
    $this->assertSession()->buttonExists(t('Add access data'));
    $this->assertSession()
      ->pageTextContains(t('Ticket global notification settings'));
    $this->assertSession()->pageTextContains(t('Mail from address'));
    $this->assertSession()->pageTextContains(t('Mail to addresses'));
    $this->assertSession()->pageTextContains(t('Mail reply addresses'));
    $this->assertSession()->pageTextContains(t('Ticket entity view settings'));
    $this->assertSession()
      ->pageTextContains(t('Open entities in a modal window'));
    $this->assertSession()->pageTextContains(t('Modal window size'));
    $this->submitForm([], t('Save configuration'));
  }

  /**
   * Tests the ticket type.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testTicketType() {
    // Overview.
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('admin/structure/ticket_types');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Name'));
    $this->assertSession()->pageTextContains(t('Description'));
    $this->assertSession()->pageTextContains(t('API'));
    $this->assertSession()->pageTextContains(t('Operations'));
    $this->assertSession()->pageTextContains(t('Default'));
    $this->assertSession()->pageTextContains(t('Bug'));
    $this->assertSession()->pageTextContains(t('None'));

    // Form
    $this->drupalGet('admin/structure/ticket_type/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Name'));
    $this->assertSession()->pageTextContains(t('Description'));
    $this->assertSession()->pageTextContains(t('API'));
    $this->assertSession()->pageTextContains(t('API Name'));
    $this->assertSession()->pageTextContains(t('Notification'));
    $this->assertSession()->pageTextContains(t('Mail to addresses'));
    $this->assertSession()->pageTextContains(t('Mail reply addresses'));

    // Entity type overview
    $this->drupalGet('admin/structure/ticket_types');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Test any'));
    $this->assertSession()->pageTextContains(t('Test any description'));
    $this->assertSession()->pageTextContains(t('Test own'));
    $this->assertSession()->pageTextContains(t('Test own description'));

    // Anonymous
    $this->drupalLogout();
    $this->drupalGet('admin/structure/ticket_types');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/structure/ticket_type/add');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/structure/ticket_types');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests the ticket type.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testTicket() {
    $this->drupalLogin($this->drupalCreateUser(['administer ticket entities']));
    $ticket = Ticket::create([
      'type'        => 'bug',
      'name'        => t('Test ticket'),
      'importance'  => t('High'),
      'description' => t('Test description'),
      'status'      => t('Open'),
    ]);
    $ticket->save();

    // Entity overview
    $this->drupalGet('admin/tickets');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Name'));
    $this->assertSession()->pageTextContains(t('Create by'));
    $this->assertSession()->pageTextContains(t('Published'));
    $this->assertSession()->pageTextContains(t('Sort by'));
    $this->assertSession()->pageTextContains(t('Order'));
    $this->assertSession()->pageTextContains(t('Changed'));
    $this->assertSession()->pageTextContains(t('Test ticket'));
    $this->assertSession()->pageTextContains(t('Bug'));

    // Add form
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('admin/ticket/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Bug'));
    $this->assertSession()->pageTextContains(t('Default'));

    // Add bug form
    $this->drupalGet('admin/ticket/add/bug');
    $this->ticketEntityFormCollection();

    // Edit bug
    $this->drupalGet(sprintf('admin/ticket/%s/edit', $ticket->id()));
    $this->ticketEntityFormCollection();

    // View bug
    $this->drupalGet('admin/ticket/' . $ticket->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('High'));
    $this->assertSession()->pageTextContains(t('Test ticket'));
    $this->assertSession()->pageTextContains(t('Test description'));
    $this->assertSession()->pageTextContains(t('Open'));
    $this->assertSession()->pageTextContains(t('Send notification mail'));

    // Edit bug
    $this->drupalGet(sprintf('admin/ticket/%s/delete', $ticket->id()));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Send notification mail'));

    // Anonymous
    $this->drupalLogout();
    $this->drupalGet('admin/tickets');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/ticket/add');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/ticket/add/bug');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/ticket/' . $ticket->id());
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check the ticket entity form.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  private function ticketEntityFormCollection() {
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Create by'));
    $this->assertSession()->pageTextContains(t('Name'));
    $this->assertSession()->pageTextContains(t('Importance'));
    $this->assertSession()->pageTextContains(t('Description'));
    $this->assertSession()->pageTextContains(t('Attachments'));
    $this->assertSession()->pageTextContains(t('Status'));
    $this->assertSession()->pageTextNotContains(t('Comments'));
    $this->assertSession()->pageTextContains(t('Published'));
    $this->assertSession()->pageTextContains(t('Send notification mail'));
  }

}
