<?php
/** @noinspection PhpHierarchyChecksInspection */

namespace Drupal\Tests\codev_ticket_system\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\RequirementsPageTrait;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: FunctionalTestBase.php
 * .
 */

/**
 * Class FunctionalTestBase.
 *
 * Default functional test base settings.
 *
 * @package      Drupal\Tests\codev_ticket_system\Functional
 *
 * @group        codev_ticket_system
 *
 * @noinspection PhpUnused
 */
abstract class FunctionalTestBase extends BrowserTestBase {

  use SchemaCheckTestTrait;
  use RequirementsPageTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_ticket_system',
    'codev_ticket_system_test',
  ];

  /**
   * An array of config object names that are excluded from schema checking.
   *
   * @var string[]
   */
  protected static $configSchemaCheckerExclusions = [
    'views.view.ticket',
  ];

}
