<?php

namespace Drupal\Tests\codev_ticket_system\Functional;

use Drupal\codev_ticket_system\Entity\Ticket;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: TicketEntityTest.php
 * .
 */

/**
 * Class TicketEntityTest.
 *
 * Tests installation module expectations.
 *
 * @package      Drupal\Tests\codev_ticket_system\Functional
 *
 * @group        codev_ticket_system
 *
 * @noinspection PhpUnused
 */
class TicketEntityPermissionTest extends FunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_ticket_system',
    'codev_ticket_system_test',
  ];

  /**
   * @var \Drupal\user\Entity\User
   */
  public $userAny;

  /**
   * @var \Drupal\user\Entity\User
   */
  public $userOwn;

  /**
   * @var \Drupal\user\Entity\User
   */
  public $userEntityTypeAny;

  /**
   * @var \Drupal\user\Entity\User
   */
  public $userEntityTypeOwn;

  /**
   * @var \Drupal\codev_ticket_system\Entity\Ticket
   */
  public $ticketAny;

  /**
   * @var \Drupal\codev_ticket_system\Entity\Ticket
   */
  public $ticketOwn;

  /**
   * @var \Drupal\codev_ticket_system\Entity\Ticket
   */
  public $ticketEntityTypeAny;

  /**
   * @var \Drupal\codev_ticket_system\Entity\Ticket
   */
  public $ticketEntityTypeOwn;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp() {
    parent::setUp();

    $this->userAny = $this->drupalCreateUser([
      'edit any ticket entities',
      'delete any ticket entities',
    ]);

    $this->userOwn = $this->drupalCreateUser([
      'edit own ticket entities',
      'delete own ticket entities',
    ]);

    $this->userEntityTypeAny = $this->drupalCreateUser([
      'edit any ticket entities test_any',
      'delete any ticket entities test_any',
    ]);

    $this->userEntityTypeOwn = $this->drupalCreateUser([
      'edit own ticket entities test_own',
      'delete own ticket entities test_own',
    ]);

    $this->ticketAny = Ticket::create([
      'name'    => t('Test any'),
      'type'    => 'test_any',
      'user_id' => $this->userAny->id(),
    ]);
    $this->ticketAny->save();

    $this->ticketOwn = Ticket::create([
      'name'    => t('Test own'),
      'type'    => 'test_own',
      'user_id' => $this->userOwn->id(),
    ]);
    $this->ticketOwn->save();

    $this->ticketEntityTypeAny = Ticket::create([
      'name'    => t('Test any'),
      'type'    => 'test_any',
      'user_id' => $this->userEntityTypeAny->id(),
    ]);
    $this->ticketEntityTypeAny->save();

    $this->ticketEntityTypeOwn = Ticket::create([
      'name'    => t('Test own'),
      'type'    => 'test_own',
      'user_id' => $this->userEntityTypeOwn->id(),
    ]);
    $this->ticketEntityTypeOwn->save();
  }

  /**
   * Tests the entity permissions.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testTicketEntityPermission() {
    // Entity collection.
    $this->drupalLogin($this->drupalCreateUser(['administer ticket entities']));
    $this->drupalGet('admin/tickets');
    $this->assertSession()->statusCodeEquals(200);

    // Anonymous
    $this->drupalLogout();
    $this->drupalGet('admin/tickets');
    $this->assertSession()->statusCodeEquals(403);

    // Create form any.
    $this->drupalLogin($this->drupalCreateUser(['add ticket entities']));
    $this->drupalGet('admin/ticket/add/test_own');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('admin/ticket/add/test_any');
    $this->assertSession()->statusCodeEquals(200);

    // Create form own.
    $this->drupalLogin($this->drupalCreateUser(['add ticket entities test_own']));
    $this->drupalGet('admin/ticket/add/test_own');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('admin/ticket/add/test_any');
    $this->assertSession()->statusCodeEquals(403);

    // Create form anonymous.
    $this->drupalLogout();
    $this->drupalGet('admin/ticket/add/test_own');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/ticket/add/test_any');
    $this->assertSession()->statusCodeEquals(403);

    // Edit operation test collection
    $this->operationPermissionsCollection('edit');

    // Delete operation test collection
    $this->operationPermissionsCollection('delete');

    // Entity view published
    $this->viewPermissionsCollection('published');

    // Set unpublished
    $this->ticketOwn->setUnpublished()->save();
    $this->ticketAny->setUnpublished()->save();
    $this->ticketEntityTypeOwn->setUnpublished()->save();
    $this->ticketEntityTypeAny->setUnpublished()->save();

    // Check if user with only published permission can not access
    $this->viewAnonymousPermissionsCollection();

    // Entity view published
    $this->viewPermissionsCollection('unpublished');

    // Anonymous
    $this->drupalLogout();
    $this->viewAnonymousPermissionsCollection();
  }

  /**
   * Test collection for entity edit or delete operations.
   *
   * @param string $operation
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function operationPermissionsCollection(string $operation) {
    // Own no type
    $ticketEntityOwnOwn = Ticket::create([
      'name'    => t('Test'),
      'type'    => 'test_own',
      'user_id' => $this->userOwn->id(),
    ]);
    $ticketEntityOwnOwn->save();
    $ticketEntityOwnAny = Ticket::create([
      'name'    => t('Test'),
      'type'    => 'test_any',
      'user_id' => $this->userOwn->id(),
    ]);
    $ticketEntityOwnAny->save();
    $this->drupalLogin($this->userOwn);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $ticketEntityOwnOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $ticketEntityOwnAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(sprintf(
      'admin/ticket/%s/%s', $this->ticketAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf(
      'admin/ticket/%s/%s', $this->ticketEntityTypeOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf(
      'admin/ticket/%s/%s', $this->ticketEntityTypeAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);

    // Own with type
    $ticketEntityOwnOwn = Ticket::create([
      'name'    => t('Test'),
      'type'    => 'test_own',
      'user_id' => $this->userEntityTypeOwn->id(),
    ]);
    $ticketEntityOwnOwn->save();
    $ticketEntityOwnAny = Ticket::create([
      'name'    => t('Test'),
      'type'    => 'test_any',
      'user_id' => $this->userEntityTypeOwn->id(),
    ]);
    $ticketEntityOwnAny->save();
    $this->drupalLogin($this->userEntityTypeOwn);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketEntityTypeOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $ticketEntityOwnOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $ticketEntityOwnAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketEntityTypeAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);

    // Any no type
    $this->drupalLogin($this->userAny);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketEntityTypeOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketEntityTypeAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);

    // Any with type
    $this->drupalLogin($this->userEntityTypeAny);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketEntityTypeOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketEntityTypeAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(200);

    // Anonymous
    $this->drupalLogout();
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketEntityTypeOwn->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet(sprintf('admin/ticket/%s/%s',
      $this->ticketEntityTypeAny->id(), $operation));
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test view permissions collection.
   *
   * @param string $view
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function viewPermissionsCollection(string $view) {
    // Entity view published no type
    $this->drupalLogin($this->drupalCreateUser([
      sprintf('view %s ticket entities', $view),
    ]));
    $this->drupalGet('admin/ticket/' . $this->ticketOwn->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('admin/ticket/' . $this->ticketAny->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('admin/ticket/' . $this->ticketEntityTypeOwn->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('admin/ticket/' . $this->ticketEntityTypeAny->id());
    $this->assertSession()->statusCodeEquals(200);

    // Entity view published with type
    $this->drupalLogin($this->drupalCreateUser([
      sprintf('view %s ticket entities test_own', $view),
    ]));
    $this->drupalGet('admin/ticket/' . $this->ticketOwn->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('admin/ticket/' . $this->ticketAny->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/ticket/' . $this->ticketEntityTypeOwn->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('admin/ticket/' . $this->ticketEntityTypeAny->id());
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test view anonymous permissions.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  private function viewAnonymousPermissionsCollection() {
    $this->drupalGet('admin/ticket/' . $this->ticketOwn->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/ticket/' . $this->ticketAny->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/ticket/' . $this->ticketEntityTypeOwn->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/ticket/' . $this->ticketEntityTypeAny->id());
    $this->assertSession()->statusCodeEquals(403);
  }

}
