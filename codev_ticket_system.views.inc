<?php

use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_alter().
 *
 * @noinspection PhpUnused
 */
function codev_ticket_system_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if (in_array($view->id(), ['comment', 'comments_recent'])) {
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query->addWhere('', 'comment_field_data.comment_type', 'ticket', '!=');
  }
}
