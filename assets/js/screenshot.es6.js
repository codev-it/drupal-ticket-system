// noinspection JSUnresolvedVariable

/**
 * @file
 * Behaviors of codev_banner module.
 */
(($, Drupal, html2canvas) => {
  const { behaviors } = Drupal;

  const Screenshot = {
    /**
     * Return the screenshot storage
     */
    imageStorageKey: 'screenshot',

    /**
     * Set the image to the storage.
     *
     * @param {string} img image base64 string
     */
    setStoredImage(img) {
      localStorage.setItem(this.imageStorageKey, img || '');
    },

    /**
     * Get the image from the storage.
     *
     * @return {string} return image base64 string
     */
    getStoredImage() {
      return localStorage.getItem(this.imageStorageKey);
    },

    /**
     * Build the screenshot data.
     */
    build() {
      html2canvas(document.body, {
        useCORS: true,
        allowTaint: true,
        logging: false,
        ignoreElements: (elem) => {
          const { height } = elem.getBoundingClientRect();
          const excludes = ['HEAD', 'META', 'TITLE', 'STYLE', 'LINK'];
          return !height && !excludes.includes(elem.nodeName);
        }
      }).then((canvas) => {
        this.setStoredImage(canvas.toDataURL());
      });
    }
  };

  /**
   * Ticket system screenshot behaviors
   *
   * @type {{attach: function}}
   */
  behaviors.codevTicketSystemScreenshot = {
    attach: () => {
      // noinspection SpellCheckingInspection
      $(window)
        .once('dialog-screenshot')
        .on({
          'dialog:beforecreate': (event, dialog, $element, settings) => {
            const { dialogClass } = settings;
            const key = Screenshot.imageStorageKey;
            if (dialogClass.split(' ').includes(key)) {
              Screenshot.setStoredImage('');
              Screenshot.build();
            }
          },
          'dialog:aftercreate': (event, dialog, $element, settings) => {
            const { dialogClass } = settings;
            const key = Screenshot.imageStorageKey;
            if (dialogClass.split(' ').includes(key)) {
              const $progress = $(
                Drupal.theme('ajaxProgressIndicatorFullscreen')
              );
              $('body').append($progress);
              const $attachments = $element.find(`[name="${key}"]`);
              const screenshotChecker = setInterval(() => {
                const data = Screenshot.getStoredImage();
                if (data !== '') {
                  $attachments.val(Screenshot.getStoredImage());
                  $progress.remove();
                  clearInterval(screenshotChecker);
                }
              }, 1);
            }
          }
        });
    }
  };
})(jQuery, Drupal, html2canvas);
