// noinspection All

/**
 * @file
 * Behaviors of codev_banner module.
 */
(function ($, Drupal, html2canvas) {
  var behaviors = Drupal.behaviors;
  var Screenshot = {
    /**
     * Return the screenshot storage
     */
    imageStorageKey: 'screenshot',

    /**
     * Set the image to the storage.
     *
     * @param {string} img image base64 string
     */
    setStoredImage: function setStoredImage(img) {
      localStorage.setItem(this.imageStorageKey, img || '');
    },

    /**
     * Get the image from the storage.
     *
     * @return {string} return image base64 string
     */
    getStoredImage: function getStoredImage() {
      return localStorage.getItem(this.imageStorageKey);
    },

    /**
     * Build the screenshot data.
     */
    build: function build() {
      var _this = this;

      html2canvas(document.body, {
        useCORS: true,
        allowTaint: true,
        logging: false,
        ignoreElements: function ignoreElements(elem) {
          var _elem$getBoundingClie = elem.getBoundingClientRect(),
              height = _elem$getBoundingClie.height;

          var excludes = ['HEAD', 'META', 'TITLE', 'STYLE', 'LINK'];
          return !height && !excludes.includes(elem.nodeName);
        }
      }).then(function (canvas) {
        _this.setStoredImage(canvas.toDataURL());
      });
    }
  };
  /**
   * Ticket system screenshot behaviors
   *
   * @type {{attach: function}}
   */

  behaviors.codevTicketSystemScreenshot = {
    attach: function attach() {
      // noinspection SpellCheckingInspection
      $(window).once('dialog-screenshot').on({
        'dialog:beforecreate': function dialogBeforecreate(event, dialog, $element, settings) {
          var dialogClass = settings.dialogClass;
          var key = Screenshot.imageStorageKey;

          if (dialogClass.split(' ').includes(key)) {
            Screenshot.setStoredImage('');
            Screenshot.build();
          }
        },
        'dialog:aftercreate': function dialogAftercreate(event, dialog, $element, settings) {
          var dialogClass = settings.dialogClass;
          var key = Screenshot.imageStorageKey;

          if (dialogClass.split(' ').includes(key)) {
            var $progress = $(Drupal.theme('ajaxProgressIndicatorFullscreen'));
            $('body').append($progress);
            var $attachments = $element.find("[name=\"".concat(key, "\"]"));
            var screenshotChecker = setInterval(function () {
              var data = Screenshot.getStoredImage();

              if (data !== '') {
                $attachments.val(Screenshot.getStoredImage());
                $progress.remove();
                clearInterval(screenshotChecker);
              }
            }, 1);
          }
        }
      });
    }
  };
})(jQuery, Drupal, html2canvas);
