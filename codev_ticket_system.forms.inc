<?php

use Drupal\codev_ticket_system\Settings;
use Drupal\codev_utils\Ajax\ReloadCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 *
 * @noinspection PhpUnused
 */
function codev_ticket_system_form_views_exposed_form_alter(&$form, FormStateInterface $form_state) {
  $storage = $form_state->getStorage();
  /** @var \Drupal\views\ViewExecutable $view */
  $view = $storage['view'];
  if ($view->id() == 'ticket' && $view->current_display == 'collection') {
    $users = [];
    $status = [];
    $database = Drupal::database();
    $query = $database->select('ticket_field_data', 't');
    $query->fields('t', ['user_id', 'status']);
    $data = $query->distinct()->execute()->fetchAll();
    foreach ($data as $item) {
      if (!in_array($item->user_id, $users)) {
        $users[] = $item->user_id;
      }
      if (!in_array($item->status, $status)) {
        $status[] = $item->status;
      }
    }

    if (!empty($form['user_id'])) {
      /** @noinspection PhpUnhandledExceptionInspection */
      $users = Drupal::entityTypeManager()
        ->getStorage('user')
        ->loadMultiple($users);
      $form['user_id']['#type'] = 'select';
      $form['user_id']['#empty_option'] = '- Any -';
      $form['user_id']['#options'] = [];
      foreach ($users as $user_id => $user) {
        $user_name = $user->label();
        $form['user_id']['#options'][$user_id] = $user_name;
      }
      unset($form['user_id']['#size']);
    }

    if (!empty($form['status'])) {
      $form['status']['#type'] = 'select';
      $form['status']['#empty_option'] = '- Any -';
      $form['status']['#options'] = array_combine($status, $status) ?: [];
      unset($form['status']['#size']);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @noinspection PhpUnused
 */
function codev_ticket_system_form_comment_ticket_form_alter(&$form, FormStateInterface $form_state) {
  /** @var \Drupal\comment\CommentForm $form_object */
  $form_object = $form_state->getFormObject();
  /** @var \Drupal\comment\Entity\Comment $comment_entity */
  $comment_entity = $form_object->getEntity();
  /** @var \Drupal\codev_ticket_system\Entity\Ticket $ticket_entity */
  $ticket_entity = $comment_entity->getCommentedEntity();
  /** @var \Drupal\codev_ticket_system\Entity\TicketType $ticket_bundle_entity */
  $ticket_bundle_entity = $ticket_entity->getBundleEntity();

  $form['#attached']['library'][] = 'codev_utils/ajax.reload-command';

  if (Drupal::request()->isXmlHttpRequest()) {
    $form['actions']['submit']['#ajax'] = [
      'callback' => '_codev_ticket_system_update_comment_ticket_form_callback',
      'wrapper'  => $form['#id'],
    ];
  }

  if ($ticket_bundle_entity->isNotificationEnable()) {
    $form['actions']['submit']['#submit'][] = '_codev_ticket_system_notification_comment_ticket_form_submit';
    $form['notification'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Send notification mail'),
      '#default_value' => TRUE,
      '#weight'        => 99,
    ];
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @noinspection PhpUnused
 */
function _codev_ticket_system_update_comment_ticket_form_callback($form, FormStateInterface $form_state) {
  if (!$form_state->hasAnyErrors()) {
    $response = new AjaxResponse();
    $response->addCommand(new ReloadCommand());
    return $response;
  }
  else {
    return $form;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_ticket_system_notification_comment_ticket_form_submit($form, FormStateInterface $form_state) {
  if ($form_state->getValue('notification')) {
    /** @var \Drupal\comment\CommentForm $form_object */
    $form_object = $form_state->getFormObject();
    /** @var \Drupal\comment\Entity\Comment $comment_entity */
    $comment_entity = $form_object->getEntity();
    /** @var \Drupal\codev_ticket_system\Entity\Ticket $ticket_entity */
    $ticket_entity = $comment_entity->getCommentedEntity();
    Settings::sendMail($ticket_entity, 'update_comment');
    $ticket_entity->save();
  }
}
